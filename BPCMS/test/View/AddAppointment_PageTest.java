/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Appointment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AFRIFA
 */
public class AddAppointment_PageTest {
    
    public AddAppointment_PageTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

  
    /**
     * Test of listAllSecretaries method, of class AddAppointment_Page.
     */
    @Test
    public void testListAllSecretaries() {
        System.out.println("listAllSecretaries");
        AddAppointment_Page.listAllSecretaries();
    }

    /**
     * Test of listAllPatients method, of class AddAppointment_Page.
     */
    @Test
    public void testListAllPatients() {
        System.out.println("listAllPatients");
        AddAppointment_Page.listAllPatients();
    }

    
}
