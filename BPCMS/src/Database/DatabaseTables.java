/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Appointment;
import Model.Staff;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author ia16abg
 */
//Using SQLite DB here. I created the table heres
public class DatabaseTables {
    static DataLayer dataLayer=new DataLayer();
     public static void main(String[] args) throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection conn = DriverManager.getConnection("jdbc:sqlite:test.db");
        Statement stat = conn.createStatement();
        //Drop Table here
   //  stat.executeUpdate("drop table if exists TREATMENT_TABLE;");
      //Creating new table
      //Patient Table
        stat.executeUpdate("CREATE TABLE IF NOT EXISTS PATIENT_TABLE (\n" +
"      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"         NAME VARCHAR(50) ,\n" +
"        PHONE VARCHAR(50) )");
        
        //Staff Table
        stat.executeUpdate("CREATE TABLE IF NOT EXISTS STAFF_TABLE (\n" +
"      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"         NAME VARCHAR(50) ,\n" +
"        PHONE VARCHAR(50),"+
"        DEPARTMENT_ID INTEGER(10))");
        
      //Department Table
        stat.executeUpdate("CREATE TABLE IF NOT EXISTS DEPARTMENT_TABLE (\n" +
"      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"         NAME VARCHAR(100) )");
        
        //Treatments Table
        stat.executeUpdate("CREATE TABLE IF NOT EXISTS TREATMENT_TABLE (\n" +
"      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"         NAME VARCHAR(100) )");
        
          //Prescriptions Table
        stat.executeUpdate("CREATE TABLE IF NOT EXISTS PRESCRIPTIONS_TABLE (\n" +
"      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"         NAME VARCHAR(100) )");
        
         //Appointments Table
         stat.executeUpdate("CREATE TABLE IF NOT EXISTS APPOINTMENTS_TABLE (\n"
                 + "      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                 + "         APPOINTMENT_DATE VARCHAR(50) ,"
                 + "        APPOINTMENT_TIME VARCHAR(50),"
                 + "         STAFF_ID INTEGER(10) ,"
                 + "        PATIENT_ID INTEGER(10) ,"
                 + "       ALLOCATED_BY_STAFF_ID INTEGER(10) ,"
                 + "        PRESCRIPTION_IDS VARCHAR(50),"
                 + "        TREATMENT_IDS VARCHAR(50),"
                 + "        NOTES VARCHAR(100),"
                 + "        STATUS INTEGER(10) DEFAULT 1 ,"
                 + "         CHECK_IN_STATUS INTEGER(10))");
        
        
        /*---------------pre defined inserts-----------*/
        //add Departments 1=secretary 2=physio 3=shopkeeper 4=manager
            //have been added
       /* dataLayer.addDepartment("Secretary");
        dataLayer.addDepartment("Physiotherapist");
        dataLayer.addDepartment("Shop Keeper");
        dataLayer.addDepartment("Manager");*/

        //add Treatments
            //have been added
      /* dataLayer.addTreatments("Mobilisation of Joints");
        dataLayer.addTreatments("Neural Mobilisation");
        dataLayer.addTreatments("Massage");
        dataLayer.addTreatments("Acupunture");
        dataLayer.addTreatments("Neurodynamics");
            dataLayer.addTreatments("Mobilisation of Spine");
        dataLayer.addTreatments("Muscle stretching");*/

        //add Prescriptions
            //have been added
       /* dataLayer.addPrescription("Paracetamol");
        dataLayer.addPrescription("Ibuprofen");
        dataLayer.addPrescription("Deep Heat");
         dataLayer.addPrescription("Exercise Mats");
        dataLayer.addPrescription("Wrist Straps");
        dataLayer.addPrescription("Morphine");*/

        //add staff physio(id=2)
        //have been added
       /* dataLayer.addStaff(new Staff(2, "Dr Xheng", "07508999995"));
        dataLayer.addStaff(new Staff(2, "Dr Brown", "08608229896"));
                dataLayer.addStaff(new Staff(2, "Dr Murphy", "08608995418"));                
     dataLayer.addStaff(new Staff(1, "Secretary Louie", "07208229774"));
          dataLayer.addStaff(new Staff(1, "Secretary Francis", "02705229259"));*/
        
       /*  ResultSet rs = stat.executeQuery("select * from STAFF_TABLE;");
        while (rs.next()) {
             System.out.println("\nStaff id = " + rs.getInt("ID"));
                     System.out.println("name = " + rs.getString("NAME"));
            System.out.println("phone = " + rs.getString("PHONE"));
               System.out.println("dept ID = " + rs.getInt("DEPARTMENT_ID"));
        }
        rs.close();
        conn.close();*/
         
       //Test Queries here
    //  Appointment appointment= new Appointment(2, 2, 3, 1, 0, "23-11-2018", "15:00", "", "2,4", "1,3");
         // Appointment appointment2= new Appointment(3, 1, 3, 1, 0, "23-11-2018", "15:00", "", "2,4", "1,3");
   // System.out.println("---> "+ dataLayer.addAppointment(appointment));
 //  System.out.println("---> "+ dataLayer.addAppointment(appointment2));
    
    // System.out.println(""+ dataLayer.getAllAppointmentsUsingDate("23-11-2018"));
    //   System.out.println(""+ dataLayer.getAllBookedPhysios_IdsUsingDateandTime("23-11-2018","12:00"));
      //   System.out.println("all physios:\n"+dataLayer.getAllAppointments());
    
       //   System.out.println("Details\n"+dataLayer.getCountsofPatientAppointmentsAndProductsUsingPatientIDandDate(1, "11"));
        
          

    }

}




//First working example
/* public static void main(String[] args) throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection conn = DriverManager.getConnection("jdbc:sqlite:test.db");
        Statement stat = conn.createStatement();
        //Drop Table here
     // stat.executeUpdate("drop table if exists PATIENT_TABLE;");
      //Creating new table
        stat.executeUpdate("CREATE TABLE IF NOT EXISTS PATIENT_TABLE (\n" +
"      ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
"         NAME VARCHAR(50) ,\n" +
"        PHONE VARCHAR(50)," +
"        BOOKING_ID INTEGER(50) DEFAULT 0)");
        

      //         PreparedStatement prep = conn.prepareStatement(
//                 "insert into PATIENT_TABLE(NAME,PHONE) values (?, ?);");
//
//         prep.setString(1, "Gandhi");
//         prep.setString(2, "0277599576");
//         //  prep.setInt(3, 0);
//         prep.addBatch();
//
//         prep.setString(1, "Turing");
//         prep.setString(2, "0500080031");
//         //   prep.setInt(3, 0);
//         prep.addBatch();
//
//         prep.setString(1, "Yaw");
//         prep.setString(2, "07003412811");
//         //   prep.setInt(3, 1);
//         prep.addBatch();
//
//         prep.setString(1, "Ntim");
//         prep.setString(2, "07503412811");
//         //   prep.setInt(3, 1);
//         prep.addBatch();
//
//        conn.setAutoCommit(false);
//        prep.executeBatch();
//        conn.setAutoCommit(true);

      /*  ResultSet rs = stat.executeQuery("select * from PATIENT_TABLE;");
        while (rs.next()) {
             System.out.println("\nid = " + rs.getInt("ID"));
                     System.out.println("name = " + rs.getString("NAME"));
            System.out.println("phone = " + rs.getString("PHONE"));
               System.out.println("booking ID = " + rs.getInt("BOOKING_ID"));
        }
        rs.close();
        conn.close();
}
}
*/