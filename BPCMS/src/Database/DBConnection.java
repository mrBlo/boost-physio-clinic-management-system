/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AFRIFA
 */
public class DBConnection {
  private static Connection connection = null;
  
   public static Connection getConnection() {
        if (connection != null) {
            return connection;
        } else {
            try {
              String driver="org.sqlite.JDBC";

                Class.forName(driver);
                connection = DriverManager.getConnection("jdbc:sqlite:test.db");
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
            return connection;
        }

    }
    
    
    
    
}
