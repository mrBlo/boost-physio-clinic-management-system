/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Model.Appointment;
import Model.Department;
import Model.DetailedAppointment;
import Model.Person;
import Model.Prescription;
import Model.Report;
import Model.Staff;
import Model.Treatment;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AFRIFA
 */
public class DataLayer {
     //Declarations

    private Connection connection;
    ResultSet rs = null;
    Statement statement = null;
    String query;
    boolean status = false;
    PreparedStatement preparedStatement;
    String output;
    
     public DataLayer() {
        connection = DBConnection.getConnection();
    }
     
     
     
     
     
     /*-----------------------------------------SELECTS-------------------------------------------------------*/
     //PATIENTS
      public ArrayList<Person> getAllPatients() {
        ArrayList<Person> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select * from PATIENT_TABLE order by Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Person RECEIPT = new Person();
                RECEIPT.setPerson_id(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                RECEIPT.setPhone_number( rs.getString("PHONE"));
               // RECEIPT.setBooking_id(rs.getInt("BOOKING_ID"));

                //add object to List
                all.add(RECEIPT);

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
           
     //get list of Patient IDs
    public ArrayList<Integer> getAllPatientsID() {
        ArrayList<Integer> all = new ArrayList<>();
        connection = DBConnection.getConnection();
        try {
            query = "Select id from PATIENT_TABLE order by Id";
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                //add object to List
                all.add(rs.getInt("ID"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }

    //get Single Patient using Patient ID
    public Person getSinglePatientUsingPatientID(int patientID) {
        Person patient = new Person();
        connection = DBConnection.getConnection();
        try {
            query = "Select * from PATIENT_TABLE where `ID`= ? order by Id";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, patientID);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                patient.setPerson_id(rs.getInt("ID"));
                patient.setName(rs.getString("NAME"));
                patient.setPhone_number(rs.getString("PHONE"));
              //  patient.setBooking_id(rs.getInt("BOOKING_ID"));

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patient;
    }
    
    
    //get Single Patient using Patient ID
    public Person getSinglePatientUsingPatientName(String patient_name) {
        Person patient = new Person();
        connection = DBConnection.getConnection();
        try {
            query = "Select * from PATIENT_TABLE where `NAME` like ? order by Id";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, "%" + patient_name + "%");
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                patient.setPerson_id(rs.getInt("ID"));
                patient.setName(rs.getString("NAME"));
                patient.setPhone_number(rs.getString("PHONE"));

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return patient;
    }
    
    

       //STAFF
      public ArrayList<Staff> getAllStaff() {
        ArrayList<Staff> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select a.ID, a.NAME,a.PHONE,b.NAME AS 'DEPARTMENT_NAME' from STAFF_TABLE a,DEPARTMENT_TABLE b "
                    + "where b.ID = a.DEPARTMENT_ID"
                    + "  order by a.Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Staff RECEIPT = new Staff();
                RECEIPT.setStaff_id(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                RECEIPT.setPhone_number( rs.getString("PHONE"));
                RECEIPT.setDepartment_name(rs.getString("DEPARTMENT_NAME"));

                //add object to List
                all.add(RECEIPT);

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
      //get list of all Physios
           public ArrayList<Staff> getAllPhysios() {
        ArrayList<Staff> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select a.ID, a.NAME,a.PHONE,b.NAME AS 'DEPARTMENT_NAME' from STAFF_TABLE a,DEPARTMENT_TABLE b "
                    + "where b.ID = a.DEPARTMENT_ID AND a.department_id = 2"
                    + "  order by a.Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Staff RECEIPT = new Staff();
                RECEIPT.setStaff_id(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                RECEIPT.setPhone_number( rs.getString("PHONE"));
                RECEIPT.setDepartment_name(rs.getString("DEPARTMENT_NAME"));

                //add object to List
                all.add(RECEIPT);

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
           
           //get list of all Secretaries
           public ArrayList<Staff> getAllSecretaries() {
        ArrayList<Staff> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select a.ID, a.NAME,a.PHONE,b.NAME AS 'DEPARTMENT_NAME' from STAFF_TABLE a,DEPARTMENT_TABLE b "
                    + "where b.ID = a.DEPARTMENT_ID AND a.department_id = 1"
                    + "  order by a.Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Staff RECEIPT = new Staff();
                RECEIPT.setStaff_id(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                RECEIPT.setPhone_number( rs.getString("PHONE"));
                RECEIPT.setDepartment_name(rs.getString("DEPARTMENT_NAME"));

                //add object to List
                all.add(RECEIPT);

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
        //get list of Staff IDs
    public ArrayList<Integer> getAllStaffIDs() {
        ArrayList<Integer> all = new ArrayList<>();
        connection = DBConnection.getConnection();
        try {
            query = "Select id from STAFF_TABLE order by Id";
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                //add object to List
                all.add(rs.getInt("ID"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
    
        //get list of Secretary IDs
    public ArrayList<Integer> getAllSecretaryIDs() {
        ArrayList<Integer> all = new ArrayList<>();
        connection = DBConnection.getConnection();
        try {
            query = "Select id from STAFF_TABLE where department_id = 1 "
                    + "order by Id";
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                //add object to List
                all.add(rs.getInt("ID"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
    
           //get list of Physio IDs
    public ArrayList<Integer> getAllPhysioIDs() {
        ArrayList<Integer> all = new ArrayList<>();
        connection = DBConnection.getConnection();
        try {
            query = "Select id from STAFF_TABLE where department_id = 2 "
                    + "order by Id";
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                //add object to List
                all.add(rs.getInt("ID"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
    
    //get Single Staff using staff ID
    public Staff getSingleStaffUsingStaffID(int staffID) {
        Staff staff = new Staff();
        connection = DBConnection.getConnection();
        try {
            query = "Select a.ID, a.NAME,a.PHONE,b.NAME AS 'DEPARTMENT_NAME' from STAFF_TABLE a,DEPARTMENT_TABLE b "
                    + "where b.ID = a.DEPARTMENT_ID"
                    + " AND a.ID= ? order by a.Id";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, staffID);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                staff.setStaff_id(rs.getInt("ID"));
                staff.setName(rs.getString("NAME"));
                staff.setPhone_number(rs.getString("PHONE"));
                staff.setDepartment_name(rs.getString("DEPARTMENT_NAME"));

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return staff;
    }
    
        //get Single Staff using staff name
    public Staff getSingleStaffUsingStaffName(String staffname) {
        Staff staff = new Staff();
        connection = DBConnection.getConnection();
        try {
            query = "Select a.ID, a.NAME,a.PHONE,b.NAME AS 'DEPARTMENT_NAME' from STAFF_TABLE a,DEPARTMENT_TABLE b "
                    + "where b.ID = a.DEPARTMENT_ID"
                    + " AND a.NAME= ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, staffname);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                staff.setStaff_id(rs.getInt("ID"));
                staff.setName(rs.getString("NAME"));
                staff.setPhone_number(rs.getString("PHONE"));
                staff.setDepartment_name(rs.getString("DEPARTMENT_NAME"));

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return staff;
    }
    
     
       //DEPARTMENT
      public ArrayList<Department> getAllDepartments() {
        ArrayList<Department> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select * from DEPARTMENT_TABLE order by Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Department RECEIPT = new Department();
                RECEIPT.setId(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                //add object to List
                all.add(RECEIPT);

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
       //TREATMENT
      public ArrayList<Treatment> getAllTreatments() {
        ArrayList<Treatment> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select * from TREATMENT_TABLE order by Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Treatment RECEIPT = new Treatment();
                RECEIPT.setId(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                //add object to List
                all.add(RECEIPT);

            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
       //get Single Treatment using Treatment ID
    public Treatment getSingleTreatmentusingTreatmentID(int TreatmentID) {
        Treatment treatment = new Treatment();
        connection = DBConnection.getConnection();
        try {
            query = "Select * from TREATMENT_TABLE where `ID`= ? order by Id";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, TreatmentID);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                treatment.setId(rs.getInt("ID"));
                treatment.setName(rs.getString("NAME"));
              
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return treatment;
    }
      
      
      
        //PRESCRIPTION
      public ArrayList<Prescription> getAllPrescriptions() {
        ArrayList<Prescription> all = new ArrayList<>();
        connection = DBConnection.getConnection();
       try {
            query = "Select * from PRESCRIPTIONS_TABLE order by Id";
            preparedStatement = connection.prepareStatement(query);
         
            rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Prescription RECEIPT = new Prescription();
                RECEIPT.setId(rs.getInt("ID"));
                RECEIPT.setName(rs.getString("NAME"));
                //add object to List
                all.add(RECEIPT);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
                 //get list of Prescription IDs
    public ArrayList<Integer> getAllPrescriptionIDs() {
        ArrayList<Integer> all = new ArrayList<>();
        connection = DBConnection.getConnection();
        try {
            query = "Select id from PRESCRIPTIONS_TABLE "
                    + "order by Id";
            preparedStatement = connection.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                //add object to List
                all.add(rs.getInt("ID"));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
         //get Single Prescription using Prescription ID
    public Prescription getSinglePrescriptionusingPrescriptionID(int PrescriptionID) {
        Prescription prescription = new Prescription();
        connection = DBConnection.getConnection();
        try {
            query = "Select * from PRESCRIPTIONS_TABLE where `ID`= ? order by Id";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, PrescriptionID);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                prescription.setId(rs.getInt("ID"));
                prescription.setName(rs.getString("NAME"));
              
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prescription;
    }
      
    
    
    
    
    
      //APPOINTMENTS
        //all appointments 
    public  ArrayList<Appointment> getAllAppointments() {
          ArrayList<Appointment> all = new ArrayList<>();       
        connection = DBConnection.getConnection();
        try {
            query = "Select * from APPOINTMENTS_TABLE order by Id DESC";
            preparedStatement = connection.prepareStatement(query);
          
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                 Appointment appointment = new Appointment();
                appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setAllocated_staff_id(rs.getInt("STAFF_ID"));
                appointment.setPatient_id(rs.getInt("PATIENT_ID"));
                appointment.setAllocated_by_staff_id(rs.getInt("ALLOCATED_BY_STAFF_ID"));
                appointment.setPrescription_ids(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatment_ids(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getInt("STATUS"));
                appointment.setCheckin_status(rs.getInt("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
                //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
    
    
      //all appointments into details
    public  ArrayList<DetailedAppointment> getAllDetailedAppointments() {
          ArrayList<DetailedAppointment> all = new ArrayList<>();       
        connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
          
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                 DetailedAppointment appointment = new DetailedAppointment();
                appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
                //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
      //all appointments using Date
    public  ArrayList<Appointment> getAllAppointmentsUsingDate(String appointmentDate) {
          ArrayList<Appointment> all = new ArrayList<>();       
        connection = DBConnection.getConnection();
        try {
            query = "Select * from APPOINTMENTS_TABLE where `APPOINTMENT_DATE`= ? order by Id DESC";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, appointmentDate);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                 Appointment appointment = new Appointment();
                appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setAllocated_staff_id(rs.getInt("STAFF_ID"));
                appointment.setPatient_id(rs.getInt("PATIENT_ID"));
                appointment.setAllocated_by_staff_id(rs.getInt("ALLOCATED_BY_STAFF_ID"));
                appointment.setPrescription_ids(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatment_ids(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getInt("STATUS"));
                appointment.setCheckin_status(rs.getInt("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
                //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
     
        //all Booked Physios using Date and Time
    public  ArrayList<Staff> getAllBookedPhysiosUsingDateandTime(String appointmentDate,String Time) {
          ArrayList<Staff> all = new ArrayList<>();
               connection = DBConnection.getConnection();
        try {
            //show all physios who have the date and time
            query = "Select b.ID,b.NAME,b.PHONE,c.NAME AS 'DEPARTMENT_NAME' "
                    + "from APPOINTMENTS_TABLE a,STAFF_TABLE b,DEPARTMENT_TABLE c "
                    + "where a.STAFF_ID = b.ID AND b.DEPARTMENT_ID = c.ID AND "                    
                    + "b.DEPARTMENT_ID = 2 AND "
                    + "a.APPOINTMENT_DATE= ? AND a.APPOINTMENT_TIME= ? "
                    + "order by b.Id DESC";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, appointmentDate);
              preparedStatement.setString(2, Time);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                 Staff staff = new Staff();
                staff.setStaff_id(rs.getInt("ID"));                
                staff.setName(rs.getString("NAME"));
                 staff.setPhone_number(rs.getString("PHONE"));
                  staff.setDepartment_name(rs.getString("DEPARTMENT_NAME"));
              
                //add object to List
                all.add(staff);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
         
    
    
           
      //all Booked Physios IDs @ specific date&time using Date and Time
    public  ArrayList<Integer> getAllBookedPhysios_IdsUsingDateandTime(String appointmentDate,String Time) {
          ArrayList<Integer> all = new ArrayList<>();
               connection = DBConnection.getConnection();
        try {
            //show all physios who do not have the date and time
            query = "Select a.STAFF_ID "
                    + "from APPOINTMENTS_TABLE a, STAFF_TABLE b "
                   + "where a.STAFF_ID= b.ID AND "
                    + "b.DEPARTMENT_ID =2 AND " //filter for physios
                   + "a.APPOINTMENT_DATE = ? AND "
                   + "a.APPOINTMENT_TIME = ? "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, appointmentDate);
             preparedStatement.setString(2, Time);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                 Staff staff = new Staff();
              int availableID= ( (rs.getInt("STAFF_ID")));
                
                //add object to List
                all.add(availableID);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
   
     
       //ids of all appointments
    public  ArrayList<Integer> getAllAppointmentIDs() {
          ArrayList<Integer> all = new ArrayList<>();
               connection = DBConnection.getConnection();
        try {
            //show all physios who do not have the date and time
            query = "Select a.ID "
                    + "from APPOINTMENTS_TABLE a "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
                    rs = preparedStatement.executeQuery();
            while (rs.next()) {
              int availableID= ( (rs.getInt("ID")));
                 //add object to List
                all.add(availableID);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
   
      
       //all appointments into details USING ID
    public  DetailedAppointment getSingleDetailedAppointmentsUsingID(int appointment_id) {
         DetailedAppointment appointment = new DetailedAppointment();
                 connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID and a.ID = ? "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, appointment_id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                  appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
              
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return appointment;
    }
      
      
             //all appointments into details using PATIENT ID
    public  ArrayList<DetailedAppointment> getPatientDetailedAppointmentsUsingPatientID(int patient_id) {
         ArrayList<DetailedAppointment> all = new ArrayList<>();
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID and a.PATIENT_ID = ? "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, patient_id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DetailedAppointment appointment = new DetailedAppointment();
                  appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
              //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
      
        //all Unchecked In appointments of Single Patient into details using PATIENT ID
    public  ArrayList<DetailedAppointment> getAllUncheckedInPatientAppointmentsUsingPatientID(int patient_id) {
         ArrayList<DetailedAppointment> all = new ArrayList<>();
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID and a.PATIENT_ID = ? "
                    + "and a.CHECK_IN_STATUS=0 " //filter to unchecked In Status
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, patient_id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DetailedAppointment appointment = new DetailedAppointment();
                  appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
              //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
       
      
          //ids of all Unchecked appointments of a SINGLE PATIENT
    public  ArrayList<Integer> getAllUncheckedAppointmentIDsofSinglePatient(int patient_id) {
          ArrayList<Integer> all = new ArrayList<>();
               connection = DBConnection.getConnection();
        try {
            //show all physios who do not have the date and time
            query = "Select a.ID "
                    + "from APPOINTMENTS_TABLE a "
                    + "where  a.PATIENT_ID = ? and a.CHECK_IN_STATUS=0 " //filter patient id and Checkin status                    
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, patient_id);
                    rs = preparedStatement.executeQuery();
            while (rs.next()) {
              int availableID= ( (rs.getInt("ID")));
                 //add object to List
                all.add(availableID);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
   
      
         //Single Unchecked In appointment of Single Patient into details using PATIENT ID and Appointment_id
    public  DetailedAppointment getUncheckedInPatientAppointmentsUsingPatientID(int patient_id,int appointment_id) {
          DetailedAppointment appointment = new DetailedAppointment();
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID and a.PATIENT_ID = ? and a.ID = ?  "
                    + "and a.CHECK_IN_STATUS=0 " //filter to unchecked In Status
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, patient_id);
              preparedStatement.setInt(2, appointment_id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
               
                  appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
             
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return appointment;
    }
      
      
             //all appointments into details using STAFF ID
    public  ArrayList<DetailedAppointment> getAllDetailedAppointmentsUsingStaffID(int staff_id) {
         ArrayList<DetailedAppointment> all = new ArrayList<>();
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID and a.STAFF_ID = ? "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, staff_id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DetailedAppointment appointment = new DetailedAppointment();
                  appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
              //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
      
      
            //ids of all appointments of a particular Physio
    public  ArrayList<Integer> getAllAppointmentIDsofSinglePhysioUsingStaffID(int staff_id) {
          ArrayList<Integer> all = new ArrayList<>();
               connection = DBConnection.getConnection();
        try {
            //show all physios who do not have the date and time
            query = "Select a.ID "
                    + "from APPOINTMENTS_TABLE a where a.STAFF_ID = ? "
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, staff_id);
                    rs = preparedStatement.executeQuery();
            while (rs.next()) {
              int availableID= ( (rs.getInt("ID")));
                 //add object to List
                all.add(availableID);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
        
   
             //all appointments into details using PATIENT ID and Date
    public  ArrayList<DetailedAppointment> getPatientDetailedAppointmentsUsingPatientIDandDate(int patient_id,String date) {
         ArrayList<DetailedAppointment> all = new ArrayList<>();
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select a.ID ,b.NAME AS 'PATIENT_NAME',c.NAME AS 'STAFF_NAME',"
                    + "a.APPOINTMENT_DATE, a.APPOINTMENT_TIME,"
                    + "(select e.NAME "
                    + "from STAFF_TABLE e,APPOINTMENTS_TABLE f where e.ID = f.ALLOCATED_BY_STAFF_ID  )"
                    + "AS 'ALLOCATED_BY_STAFF' ,"
                    + "a.PRESCRIPTION_IDS, a.TREATMENT_IDS,"
                    + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"
                    + "case "
                    + "when a.CHECK_IN_STATUS=0 then 'Not Checked In'"
                    + "when a.CHECK_IN_STATUS=1 then 'Checked In'"
                    + "end as 'CHECK_IN_STATUS',"
                    + " a.NOTES "
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b,STAFF_TABLE c "
                    + "where a.PATIENT_ID = b.ID and a.STAFF_ID = c.ID and a.PATIENT_ID = ? "
                    + "and a.APPOINTMENT_DATE like ?" //filter with date
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, patient_id);
               preparedStatement.setString(2, "%-" + date + "-%");
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                DetailedAppointment appointment = new DetailedAppointment();
                  appointment.setId(rs.getInt("ID"));
                appointment.setDate(rs.getString("APPOINTMENT_DATE"));
                appointment.setTime(rs.getString("APPOINTMENT_TIME"));
                appointment.setStaff_name(rs.getString("STAFF_NAME"));
                appointment.setPatient_name(rs.getString("PATIENT_NAME"));
                appointment.setAllocated_by_staff_name(rs.getString("ALLOCATED_BY_STAFF"));
                appointment.setPrescriptions(rs.getString("PRESCRIPTION_IDS"));
                appointment.setTreatments(rs.getString("TREATMENT_IDS"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setCheckin_status(rs.getString("CHECK_IN_STATUS"));
                appointment.setComments(rs.getString("NOTES"));
              //add object to List
                all.add(appointment);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    } 
    
    
    /////******COUNTS OF APPOINTMENTS
                 //count of all appointments using PATIENT ID and Date
    public int getCountofPatientAppointmentsUsingPatientIDandDate(int patient_id,String date) {
         //ArrayList<DetailedAppointment> all = new ArrayList<>();
         int count=0;
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select count(a.ID ) as 'NO_OF_APPOINTMENTS' "
                     + "from APPOINTMENTS_TABLE a "
                    + "where a.PATIENT_ID = ? "
                    + "and a.APPOINTMENT_DATE like ?" //filter with date
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
             preparedStatement.setInt(1, patient_id);
               preparedStatement.setString(2, "%-" + date + "-%");
            rs = preparedStatement.executeQuery();
           count = rs.getInt("NO_OF_APPOINTMENTS");
           
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    } 
    
    
        //Improved and Combined Count Method
    /*count of all appointments,all attended/checkied_In, all missed, all cancelled,all products ordered
        ------   by using PATIENT ID and Date*/
    public ArrayList<Report> getCountsofPatientAppointmentsAndProductsUsingPatientIDandDate(int patient_id, String date) {
        ArrayList<Report> all = new ArrayList<>();
        connection = DBConnection.getConnection();
        connection = DBConnection.getConnection();
        try {
            query = "Select a.PATIENT_ID,b.NAME AS 'PATIENT_NAME', count(a.ID ) as 'NO_OF_APPOINTMENTS',"
                    + "(select count (a.ID) from APPOINTMENTS_TABLE a where a.STATUS=0 and"//filter to get closed appointments
                    + " a.PATIENT_ID = ? and a.APPOINTMENT_DATE like ?) as 'NO_OF_ATTENDED', "
                    + "(select count (a.ID) from APPOINTMENTS_TABLE a where  a.STATUS=2 and "
                    + " a.PATIENT_ID = ? and a.APPOINTMENT_DATE like ?) as 'NO_OF_MISSED',"
                    + "(select count (a.ID) from APPOINTMENTS_TABLE a where  a.STATUS=3 and "
                    + " a.PATIENT_ID = ? and a.APPOINTMENT_DATE like ?) as 'NO_OF_CANCELLED',"
                    + "(select count (a.PRESCRIPTION_IDS) from APPOINTMENTS_TABLE a "
                    + "where a.PATIENT_ID = ? and a.APPOINTMENT_DATE like ?) as 'NO_OF_ORDERED_PRESCRIPTIONS'"
                    /*
                 + "case "
                    + "when a.STATUS=0 then 'Closed' "
                    + "when a.STATUS=1 then 'Open' "
                    + "when a.STATUS=2 then 'Missed' "
                    + "when a.STATUS=3 then 'Cancelled'"
                    + "end as 'STATUS',"*/
                    + "from APPOINTMENTS_TABLE a,PATIENT_TABLE b "
                    + "where a.PATIENT_ID = b.ID and "
                    + "a.PATIENT_ID = ? "
                    + "and a.APPOINTMENT_DATE like ?" //filter with date
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, patient_id);
            preparedStatement.setString(2, "%-" + date + "-%");
            preparedStatement.setInt(3, patient_id);
            preparedStatement.setString(4, "%-" + date + "-%");
            preparedStatement.setInt(5, patient_id);
            preparedStatement.setString(6, "%-" + date + "-%");
            preparedStatement.setInt(7, patient_id);
            preparedStatement.setString(8, "%-" + date + "-%");
            preparedStatement.setInt(9, patient_id);
            preparedStatement.setString(10, "%-" + date + "-%");
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setPatientId(rs.getInt("PATIENT_ID"));
                report.setPatientName(rs.getString("PATIENT_NAME"));
                report.setNo_of_appointments(rs.getInt("NO_OF_APPOINTMENTS"));
                report.setNo_of_attended(rs.getInt("NO_OF_ATTENDED"));
                report.setNo_of_missed(rs.getInt("NO_OF_MISSED"));
                report.setNo_of_cancelled(rs.getInt("NO_OF_CANCELLED"));
                report.setNo_of_products_ordered(rs.getInt("NO_OF_ORDERED_PRESCRIPTIONS"));

                //add object to List
                all.add(report);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }

    
                   //count of all appointments using PATIENT ID and Date
    public int getTotalCountofPatientAppointmentsUsingDate(String date) {
         //ArrayList<DetailedAppointment> all = new ArrayList<>();
         int count=0;
               connection = DBConnection.getConnection(); 
                         connection = DBConnection.getConnection();
        try {
            query = "Select count(a.ID ) as 'NO_OF_APPOINTMENTS' "
                     + "from APPOINTMENTS_TABLE a "
                    + "where "
                    + "a.APPOINTMENT_DATE like ?" //filter with date
                    + "order by a.Id DESC";
            preparedStatement = connection.prepareStatement(query);
                          preparedStatement.setString(1, "%-" + date + "-%");
            rs = preparedStatement.executeQuery();
           count = rs.getInt("NO_OF_APPOINTMENTS");
           
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    } 
    
    
      
     
     
     
     

     /*---------------------------------------- INSERTS---------------------------------------*/
                     //PATIENTS
     public boolean addPatient(Person person) {
         connection = DBConnection.getConnection();
         try {
            preparedStatement = connection
                    .prepareStatement("insert into PATIENT_TABLE(NAME,PHONE) values (?, ?);");
            // Parameters start with 1
            preparedStatement.setString(1,person.getName() );
            preparedStatement.setString(2, person.getPhone_number());
            status = preparedStatement.executeUpdate() > 0;
           // status=rs.next();
               preparedStatement.close();
         
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return status;
    }
  
               //STAFF
     public boolean addStaff(Staff staff) {
         connection = DBConnection.getConnection();
         try {
            preparedStatement = connection
                    .prepareStatement("insert into STAFF_TABLE(NAME,PHONE,DEPARTMENT_ID) values (?, ?, ?);");
            // Parameters start with 1
            preparedStatement.setString(1,staff.getName() );
            preparedStatement.setString(2, staff.getPhone_number());
              preparedStatement.setInt(3, staff.getDepartment_id());
            status = preparedStatement.executeUpdate() > 0;
           // status=rs.next();
               preparedStatement.close();
         
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return status;
    }
     
        //DEPARTMENTS
     public boolean addDepartment(String departmentName) {
         connection = DBConnection.getConnection();
         try {
            preparedStatement = connection
                    .prepareStatement("insert into DEPARTMENT_TABLE(NAME) values ( ?);");
            // Parameters start with 1
            preparedStatement.setString(1,departmentName );
            status = preparedStatement.executeUpdate() > 0;
           // status=rs.next();
               preparedStatement.close();
                 } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return status;
    }
        
//TREATMENTS
     public boolean addTreatments(String treatmentName) {
         connection = DBConnection.getConnection();
         try {
            preparedStatement = connection
                    .prepareStatement("insert into TREATMENT_TABLE(NAME) values ( ?);");
            // Parameters start with 1
            preparedStatement.setString(1,treatmentName );
            status = preparedStatement.executeUpdate() > 0;
           // status=rs.next();
               preparedStatement.close();
                 } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return status;
    }
      
     
//PRESCRIPTIONS
     public boolean addPrescription(String prescriptionName) {
         connection = DBConnection.getConnection();
         try {
            preparedStatement = connection
                    .prepareStatement("insert into PRESCRIPTIONS_TABLE(NAME) values ( ?);");
            // Parameters start with 1
            preparedStatement.setString(1,prescriptionName );
            status = preparedStatement.executeUpdate() > 0;
           // status=rs.next();
               preparedStatement.close();
                 } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return status;
    }
     
     
               //APPOINTMENTS
     public boolean addAppointment(Appointment appointment) {
         connection = DBConnection.getConnection();
         try {
            preparedStatement = connection
                    .prepareStatement("insert into APPOINTMENTS_TABLE(APPOINTMENT_DATE,APPOINTMENT_TIME,STAFF_ID,PATIENT_ID,"
                            + "ALLOCATED_BY_STAFF_ID,PRESCRIPTION_IDS,TREATMENT_IDS,STATUS,CHECK_IN_STATUS) values "
                            + "(?, ?, ? ,?, ?, ?, ?, ?, ?);");
            // Parameters start with 1
             preparedStatement.setString(1, appointment.getDate());
             preparedStatement.setString(2, appointment.getTime());
             preparedStatement.setInt(3, appointment.getAllocated_staff_id());
             preparedStatement.setInt(4, appointment.getPatient_id());
              preparedStatement.setInt(5, appointment.getAllocated_by_staff_id());
              preparedStatement.setString(6, appointment.getPrescription_ids());
              preparedStatement.setString(7, appointment.getTreatment_ids());
              preparedStatement.setInt(8, appointment.getStatus());
               preparedStatement.setInt(9, appointment.getCheckin_status());
            status = preparedStatement.executeUpdate() > 0;
           // status=rs.next();
               preparedStatement.close();
         
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return status;
    }
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
         //*------------------------------------ UPDATES -----------------------------------------------*/
    //  update patient
    public boolean updatePatient(int patient_id, String name, String phone) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `PATIENT_TABLE` SET `NAME`= ?,`PHONE`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, phone);
            preparedStatement.setInt(3, patient_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update patient name using Id
    public boolean updatePatientNameUsingID(int patient_id, String name) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `PATIENT_TABLE` SET `NAME`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, patient_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }
    //  update patient phone using Id

    public boolean updatePatientPhoneUsingID(int patient_id, String phone) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `PATIENT_TABLE` SET `PHONE`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, phone);
            preparedStatement.setInt(2, patient_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Date using Id
    public boolean updateAppointmentDateUsingID(int appointment_id, String date) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `APPOINTMENT_DATE`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, date);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Date using Id
    public boolean updateAppointmentTimeUsingID(int appointment_id, String time) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `APPOINTMENT_TIME`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, time);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Status using Id
    public boolean updateAppointmentStatusUsingID(int appointment_id, int appointment_status) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `STATUS`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, appointment_status);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Check_In using Id 
    public boolean updateAppointmentCheckInUsingID(int appointment_id, int check_in) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `CHECK_IN_STATUS`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, check_in);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Check_In using Id and set Status to 0 = closed
    public boolean updateAppointmentCheckInAndCloseAppointmentUsingID(int appointment_id, int check_in) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `CHECK_IN_STATUS`= ?, "
                    + "STATUS= 0" //close appointment here
                    + " WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, check_in);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Notes using Id
    public boolean updateAppointmentNotesUsingID(int appointment_id, String notes) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `NOTES`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, notes);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    //  update Appointment Prescription using Id
    public boolean updateAppointmentPrescriptionUsingID(int appointment_id, String prescription) {
        connection = DBConnection.getConnection();
        try {

            String sql = "UPDATE `APPOINTMENTS_TABLE` SET `PRESCRIPTION_IDS`= ? WHERE `ID` = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, prescription);
            preparedStatement.setInt(2, appointment_id);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }
    
    
    
    
    

    ///*------------------------ DELETES -----------------------------------*///
    //Delete Appointment using appointment id
    public boolean deleteAppointmentUsingID(int appointmentID) {
        connection = DBConnection.getConnection();
        try {
            String sql = "delete from `APPOINTMENTS_TABLE` where `ID` = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, appointmentID);
//            int affectedRecords = preparedStatement.executeUpdate();
            //System.out.println("Number of deleted records:- " + affectedRecords);
            status = preparedStatement.executeUpdate() > 0;
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }
 
      
      
      
      
      
      
}

 
    
