/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.Appointment;
import Model.DetailedAppointment;
import Model.Staff;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author AFRIFA
 */
public class ViewAppointment_Page {

    static Scanner input = new Scanner(System.in);
    static ArrayList<DetailedAppointment> allAppointmentsArrayList;
    static DetailedAppointment appointment;
    private static String[] args;

    public static void main(String[] args) {
        listAllAppointments();
        int selection = editOrDelete();

        //handle selection
        switch (selection) {
            case 1:
                editMethod();
                break;
            case 2:
                deleteMethod();
                break;
            case 0:
                      //Navigate back to Secretary Page
                Secretary_Page.main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                //navigate to top
                main(args);
                break;
        }

    }

    //list all appointments method
    public static void listAllAppointments() {
        //get all appointments from DataLayer
        allAppointmentsArrayList = IoC.dataLayer.getAllDetailedAppointments();
          System.out.println("******************************************** LIST OF ALL APPOINTMENTS ****************************************************");
            System.out.println("---------------------------------------------------------------------------------------------------"
                    + "------------------------------------------------------------------------------------------------");
             //check for empty List
        if (allAppointmentsArrayList.isEmpty()) {
            System.out.println("-Empty Appointment List");
            System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
        }
        else{
        System.out.printf("%5s %10s %20s %20s %20s %10s %25s %20s %20s %20s %20s %25s",
                    "NO.", "APPOINTMENT ID","PATIENT NAME","PHYSIOTHERAPIST","APPOINTMENT DATE", "TIME",
                    "ASSIGNED BY", "CHECK IN STATUS", "STATUS","NOTES", "PRODUCTS", "TREATMENT");
            System.out.println();
             System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
             
           int i = 1;
            for (DetailedAppointment appointment2 : allAppointmentsArrayList) {
                System.out.format("%5d %10d %20s %20s %20s %15s %25s %20s %20s %20s %20s %25s",
                        i,appointment2.getId(),appointment2.getPatient_name(),appointment2.getStaff_name(),
                        appointment2.getDate(), appointment2.getTime(), appointment2.getAllocated_by_staff_name(),
                        appointment2.getCheckin_status(),appointment2.getStatus(),appointment2.getComments(),
                       appointment2.getPrescriptions(), appointment2.getTreatments() );
                i++;
                System.out.println();
            }
           System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
    }
    }
    
    //Edit or Delete Question
    public static int editOrDelete() {
        int answer = 10;
        System.out.println("\nKindly choose what you want to do:\n"
                + "0.Go Back to Main Secretary Page\n"
                + "1.Edit Appointment\n"
                + "2.Delete Appointment"
                );
        //Ensures a no is entered.
        do {
            System.out.println("\n-Enter either 0,1 or 2:  ");
              System.out.print("> ");
            answer = input.nextInt();
        } while (answer == 10);

        return answer;
    }

    //edit Appointment method
    public static void editMethod() {
        int id, selection = 0;
        do {
            System.out.println("From the list of all Appointments\n"
                    + "Kindly Enter id of appointment you want to edit:");
              System.out.print("> ");
            id = input.nextInt();
        } while (id == 0);
        //check if input is an appintment id
        if (!IoC.dataLayer.getAllAppointmentIDs().contains(id)) {
            System.out.println("-Enter a valid Appointment ID");
            //navigate back to top
            editMethod();
        } else {//valid id
            //get detailed appointment using id
            appointment = IoC.dataLayer.getSingleDetailedAppointmentsUsingID(id);
             System.out.println("******************************************** SELECTED APPOINTMENT ****************************************************");
            System.out.println("---------------------------------------------------------------------------------------------------"
                    + "------------------------------------------------------------------------------------------------");
            System.out.printf("%10s %20s %20s %20s %10s %25s %20s %20s %20s %20s %25s",
                   "APPOINTMENT ID","PATIENT NAME","PHYSIOTHERAPIST","APPOINTMENT DATE", "TIME",
                    "ASSIGNED BY", "CHECK IN STATUS", "STATUS","NOTES", "PRODUCTS", "TREATMENT");
            System.out.println();
             System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
            System.out.format("%11d %20s %20s %20s %15s %25s %20s %20s %20s %20s %25s",
                        appointment.getId(),appointment.getPatient_name(),appointment.getStaff_name(),
                        appointment.getDate(), appointment.getTime(), appointment.getAllocated_by_staff_name(),
                        appointment.getCheckin_status(),appointment.getStatus(),appointment.getComments(),
                       appointment.getPrescriptions(), appointment.getTreatments() );
                System.out.println("\n----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");           
           // System.out.println("Selected Appointment: " + appointment); replaced with above sysout
            do {
                System.out.println("\nKindly choose what parameter you want to edit: \n"
                        + "1. Appointment Date\n"
                        + "2. Appointment Time\n"
                        + "3. Status (Cancel,Open or Close Appointment)\n"
                        + "4. Check In ");
                  System.out.print("> ");
                selection = input.nextInt();
            } while (selection == 0);
            //handle selections
            editParam(selection);

        }

    }

    //handling and edittin Params
    public static void editParam(int selection) {
        switch (selection) {
            case 1://date
                dateedit();
                break;
            case 2://time
                timeedit();
                break;
            case 3://status
                statusedit();
                break;
            case 4://check in
                checkInedit();
                break;
            default:
                System.out.println("-Invalid Selection");
                editMethod();
                break;
        }

    }

    //date edit
    public static void dateedit() {
        String appointmentDate = "";
        //Ensures a Date is entered.
        do {
            System.out.println("\nKindly Enter Date in the format: DD-MM-YYYY eg.21-11-2016");
              System.out.print("> ");
            appointmentDate = input.next();
        } while (appointmentDate.isEmpty() || appointmentDate.length() != 10);
        //set date to appointment
        appointment.setDate(appointmentDate);
        //update query here
        boolean update = IoC.dataLayer.updateAppointmentDateUsingID(appointment.getId(), appointment.getDate());
        if (update) {
            System.out.println("-Appointment Date Updated");
            confirmMsg();
        } else {
            System.out.println("-Appointment Date Not Updated");
            confirmMsg();
        }
    }

    //time edit
    public static void timeedit() {
        String appointmentTime = "";
        System.out.println("\nEnter Appointment time in the 24 hr format: eg.21:30 ");
          System.out.print("> ");
        appointmentTime = input.nextLine();
        while (!appointmentTime.contains(":") || appointmentTime.length() != 5) {
            System.out.println("-Kindly enter valid time: eg. 21:30");
              System.out.print("> ");
            appointmentTime = input.nextLine();
        }
        //Check If Time is already booked        
        //if list contains physioID then he is booked
        Staff physio= IoC.dataLayer.getSingleStaffUsingStaffName(appointment.getStaff_name());
                if (IoC.dataLayer
                        .getAllBookedPhysios_IdsUsingDateandTime(appointment.getDate(), appointmentTime)
                        .contains(physio.getStaff_id())) {
                    //he is booked 
                    System.out.println("-Sorry,Physiotherapist Is Booked at this time : "+appointmentTime);
                    //choose another time
                    System.out.println("-Choose another time");
                    //navigate to top
                    timeedit();
                }
        
        
                else{//else he is available
        //set Time to Object
        appointment.setTime(appointmentTime);
        //update query here
        boolean update = IoC.dataLayer.updateAppointmentTimeUsingID(appointment.getId(), appointment.getTime());
        if (update) {
            System.out.println("-Appointment Time Updated");
            confirmMsg();
        } else {
            System.out.println("-Appointment Time Not Updated");
            confirmMsg();
        }
                }
    }

    //status edit
    public static void statusedit() {
        int status = 0;
        System.out.println("\nEnter Status you want to assign to this appointment\n"
                + "1. Close Appointment\n"
                + "2. Open Appointment\n"
                + "3. Miss Appointment\n"
                + "4. Cancel Appointment");
          System.out.print("> ");
        status = input.nextInt();
        while (status == 0) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            status = input.nextInt();
        }
        switch (status) {
            case 1:
                //set status to 0 --close
                status = 0;
                break;
            case 2:
                //set status to 1 --open
                status = 1;
                break;
            case 3:
                //set status to 2 --missed
                status = 2;
                break;
            case 4:
                //set status to 3--cancelled
                status = 3;
                break;
            default:
                System.out.println("-Invalid Selection");
                //navigate to top
                statusedit();
                break;
        }
        //set status to Object
        appointment.setStatus(status + "");
        //update query here
        boolean update = IoC.dataLayer.updateAppointmentStatusUsingID(appointment.getId(),
                Integer.parseInt(appointment.getStatus()));
        if (update) {
            System.out.println("-Appointment Status Updated");
            confirmMsg();
        } else {
            System.out.println("-Appointment Status Not Updated");
            confirmMsg();
        }

    }

    //checkin edit
    public static void checkInedit() {
        int checkin = 0;
        System.out.println("\nEnter Check In Status you want to assign to this appointment\n"
                + "1. Check In\n"
                + "2. Check Out\n"
        );
          System.out.print("> ");
        checkin = input.nextInt();
        while (checkin == 0) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            checkin = input.nextInt();
        }
        switch (checkin) {
            case 1:
                //set checkin to 1 --check in
                appointment.setCheckin_status(1 + "");
                break;
            case 2:
                //set checkin to 0 --set to default
                appointment.setCheckin_status(0 + "");
                break;
            default:
                System.out.println("-Invalid Selection");
                //navigate to top
                checkInedit();
                break;
        }
        //set status to Object
        appointment.setCheckin_status(checkin + "");
        //update query here
        boolean update = IoC.dataLayer.updateAppointmentCheckInUsingID(appointment.getId(),
                Integer.parseInt(appointment.getCheckin_status()));
        if (update) {
            System.out.println("-Appointment Check-In Status Updated");
            confirmMsg();
        } else {
            System.out.println("-Appointment Check-In Status Not Updated");
            confirmMsg();
        }

    }

    //Confirm Method
    public static void confirmMsg() {
        int confirm = 10;

        System.out.println("\nDo you want :\n"
                + "0. Go Back to Main Secretary Page\n"
                + "1. Perform another Edit");
        while (confirm == 10) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            confirm = input.nextInt();
        }
        switch (confirm) {
            case 0:
                //Navigate back to Secretary Page
                Secretary_Page.main(args);
                break;
            case 1:
                //Navigate back to Top
                main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                confirmMsg();
                break;
        }

    }

    private static void deleteMethod() {
     int id, selection = 0;
        do {
            System.out.println("From the list of all Appointments\n"
                    + "Kindly Enter id of appointment you want to delete:");
              System.out.print("> ");
            id = input.nextInt();
        } while (id == 0);
        //check if input is an appintment id
        if (!IoC.dataLayer.getAllAppointmentIDs().contains(id)) {
            System.out.println("-Enter a valid Appointment ID");
            //navigate back to top
            deleteMethod();
        } else {//valid id
            //get detailed appointment using id
            appointment = IoC.dataLayer.getSingleDetailedAppointmentsUsingID(id);
              System.out.println("******************************************** SELECTED APPOINTMENT ****************************************************");
            System.out.println("---------------------------------------------------------------------------------------------------"
                    + "------------------------------------------------------------------------------------------------");
            System.out.printf("%10s %20s %20s %20s %10s %25s %20s %20s %20s %20s %25s",
                   "APPOINTMENT ID","PATIENT NAME","PHYSIOTHERAPIST","APPOINTMENT DATE", "TIME",
                    "ASSIGNED BY", "CHECK IN STATUS", "STATUS","NOTES", "PRODUCTS", "TREATMENT");
            System.out.println();
             System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
            System.out.format("%11d %20s %20s %20s %15s %25s %20s %20s %20s %20s %25s",
                        appointment.getId(),appointment.getPatient_name(),appointment.getStaff_name(),
                        appointment.getDate(), appointment.getTime(), appointment.getAllocated_by_staff_name(),
                        appointment.getCheckin_status(),appointment.getStatus(),appointment.getComments(),
                       appointment.getPrescriptions(), appointment.getTreatments() );
                System.out.println("\n----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");           
            
            //System.out.println("Selected Appointment: " + appointment); replaced by above display
            //delete query here
           boolean delete= IoC.dataLayer.deleteAppointmentUsingID(id);
            if (delete) {
                System.out.println("-Appointment of ID : "+appointment.getId()+" deleted successfully");
            }
            else{
            System.out.println("-Appointment Not Deleted due to Error");
            }
//back to confirm method
confirmMsg();
        }
       
        
    }

}
