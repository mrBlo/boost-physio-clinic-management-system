/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.DetailedAppointment;
import Model.Person;
import java.util.Scanner;

/**
 *
 * @author AFRIFA
 */
public class Patient_CheckIn {
     static Scanner input = new Scanner(System.in);
    static Person patient;
    private static String[] args;
      static DetailedAppointment appointment;
    
    
    public static void main(String[] args) {
           //Call list of all patients
        listAllPatients();
        //Call Method from Patient_View Details
      int patientID = patientIdEnquiry();

         patient = IoC.dataLayer.getSinglePatientUsingPatientID(patientID);
        //check for match
        if (patient.isEmpty(patient)) {
            System.out.println("-No Such Patient Name as " + patient.getName().toUpperCase());
            //navigate to top
            main(args);
        } else {
             System.out.println("\n-Welcome " + patient.getName());
             System.out.println("******************************************** RECORD OF ALL UNCHECKED PATIENT APPOINTMENTS ****************************************************");
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------");
            
             //check for when Patient Has No unchecked appointments
            if ( IoC.dataLayer.getAllUncheckedInPatientAppointmentsUsingPatientID(patient.getPerson_id()).isEmpty()) {
                System.out.println("-No Unchecked Appointments for Patient :"+patient.getName());
                  System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------");
           //ask confirm msg
                 confirmMsg() ;
            }
                         
            else{
            System.out.printf("%5s %10s %20s %20s %20s %20s %20s %20s", "NO.", " ID","APPOINTMENT DATE", "APPOINTMENT TIME", "PHYSIOTHERAPIST", "TREATMENT", "STATUS", "CHECK IN STATUS");
            System.out.println();
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------");
           
            int i = 1;
            for (DetailedAppointment appointment2 : IoC.dataLayer.getAllUncheckedInPatientAppointmentsUsingPatientID(patient.getPerson_id())) {
                System.out.format("%5d %10d %20s %20s %20s %20s %20s %20s",
                        i,appointment2.getId(), appointment2.getDate(), appointment2.getTime(), appointment2.getStaff_name(), appointment2.getTreatments(), appointment2.getStatus(),appointment2.getCheckin_status());
                i++;
                System.out.println();
            }
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------");
           
        //call edit method
            editQuestion();
                     }
   }
    }
    
       //list all Patients
    public static void listAllPatients() {

        System.out.println("************* LIST OF ALL PATIENTS *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s", "NO", "PATIENT ID", "PATIENT NAME");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Person patient2 : IoC.dataLayer.getAllPatients()) {
            System.out.format("%5d %10s %20s ",
                    i, patient2.getPerson_id(), patient2.getName());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");
    }
    
    
      //Patient ID Enquiry
    public static int patientIdEnquiry() {
        int IDinput = 0;
        System.out.println("\nFrom the above list,\nKindly Enter your Patient ID");
        while (IDinput==0) {
            System.out.println("-Kindly enter valid ID");
              System.out.print("> ");
            IDinput = input.nextInt();
        }
        return IDinput;
    }
    
    
     //Edit or Delete Question
    public static void editQuestion() {
       int id, selection = 0;
        do {
            System.out.println("\nFrom the above list of Appointments\n"
                    + "Kindly Enter id of appointment you want to Check In:");
              System.out.print("> ");
            id = input.nextInt();
        } while (id == 0);
        //check if input is an unchecked appointment id of the user
        if (!IoC.dataLayer.getAllUncheckedAppointmentIDsofSinglePatient(patient.getPerson_id()).contains(id)) {
            System.out.println("-Enter a valid and unchecked Appointment ID of the user");
            //navigate back to top
          editQuestion();
        } else {//valid id
           
           // //update query here //Update Check In Status and set Status to 0 = closed
             boolean update = IoC.dataLayer.updateAppointmentCheckInAndCloseAppointmentUsingID(id, 1);
              if (update) {
                   //get detailed appointment using id and patient id
          //  appointment = IoC.dataLayer.getUncheckedInPatientAppointmentsUsingPatientID(patient.getPerson_id(),id);
            appointment =  IoC.dataLayer.getSingleDetailedAppointmentsUsingID(id);
          System.out.println("*************** SELECTED APPOINTMENT DETAILS ******************");
            System.out.println("--------------------------------------------------");
            System.out.format("%20s %s %n", "Appointment ID: ", appointment.getId());
            System.out.format("%20s %s %n", "Appointment Date: ", appointment.getDate());
            System.out.format("%20s %s %n", "Appointment Time: ", appointment.getTime());
             System.out.format("%20s %s %n", "Staff Name: ",  appointment.getStaff_name());
            System.out.format("%20s %s %n", "Treatment: ", appointment.getTreatments());
            System.out.format("%20s %s %n", "Status: ", appointment.getStatus());
             System.out.format("%20s %s %n", "Check In Status: ", appointment.getCheckin_status());
                        
            System.out.println("--------------------------------------------------");

            System.out.println("-Appointment Date Updated");
            confirmMsg();
        } else {
            System.out.println("-Appointment Date Not Updated");
            confirmMsg();
        }
           

        }
    }
    
    
    
    
     //Confirm Method
    public static void confirmMsg() {
        int confirm = 10;

        System.out.println("\nDo you want :\n"
                + "0. Go Back to Main Patient Page\n"
                + "1. Restart CheckIn Process"
                );
        while (confirm == 10) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            confirm = input.nextInt();
        }
        switch (confirm) {
            case 0:
                //Navigate back to Patient Page
                Patient_Page.main(args);
                break;
            case 1:
                //Navigate back to Top
                main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                confirmMsg();
                break;
        }
  
}
}