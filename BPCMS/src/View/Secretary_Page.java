/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
public class Secretary_Page {
   //Constructor
    public Secretary_Page() {
    }
    
 
     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int user_input = 10;
     
        //print Welcome Msg
       welcomeMessageForSecretary();
       
         //Ensure User enters preferred functionality
        System.out.println("\nEnter your Choice: ");
          System.out.print("> ");
         user_input = input.nextInt();

            //Validating Input
            while (user_input < 0 || user_input > 4) {
                System.out.println("Enter a valid number: ");
                  System.out.print("> ");
                user_input = input.nextInt();
            }
            //Call secretary choice in Secretary Page
            Secretary_Page.userChoiceForSecretary(user_input);
            
               //ADD PATIENT 
           if(user_input==1){
               //Navigate to Secretary Page
                 AddPatient_Page.main(args);
           
           }
           
              //VIEW PATIENTS 
           if(user_input==2){
               //Navigate to Secretary Page
                 ViewPatient_Page.main(args);
           
           }
           
            
              //ADD APPOINTMENTS
           if(user_input==3){
               //Navigate to Secretary Page
                 AddAppointment_Page.main(args);
           
           }
           
               //VIEW APPOINTMENTS
           if(user_input==4){
               //Navigate to Secretary Page
                 ViewAppointment_Page.main(args);
           
           }
           
         
     
     }
    
     
     
     //Welcome Message 
       public static void welcomeMessageForSecretary(){
        System.out.println("\nChoose what function you want to perform"
                  + "\n0. Go Back"
                 + "\n1. Add Patient"
                + "\n2. View Patients"           
                  + "\n------ APPOINTMENT SECTION ----"
                 + "\n3. Book Appointment"
                + "\n4. View Appointment"
                 
               
               );
    }
       //user Choice 
        public static String userChoiceForSecretary(int input){
        switch (input) {
            case 1:
                System.out.println("\n---- ADD PATIENT PAGE ------");
                break;
            case 2:
                System.out.println("\n---- VIEW PATIENTS PAGE------");
                break;
              case 3:
                System.out.println("\n---- BOOK APPOINTMENT PAGE ------");
                break;
           case 4:
                System.out.println("\n---- VIEW APPOINTMENT PAGE ------");
                break;
             case 0:
                System.out.println("\n- Exited");
                    String[] args = new String[10];
                //Navigate back to Home Page
                HomePage.main(args);
                break;
            default:
                System.out.println("\nInvalid Choice");
                break;
        }
        return "";
        }
        
}
