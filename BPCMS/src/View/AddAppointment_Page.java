/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.Appointment;
import Model.Person;
import Model.Staff;
import Model.Treatment;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author AFRIFA
 */
public class AddAppointment_Page {

    static Scanner input = new Scanner(System.in);
    static Appointment appointment = new Appointment();
    static ArrayList<Integer> IDsList;
    static ArrayList<Person> allPatientsArrayList;
    static ArrayList<Integer> allPhysioIDsList;
    static ArrayList<Treatment> allTreatments;
    static ArrayList<Appointment> allAppointmentsArrayList;
    static String[] args = new String[10];
    static Staff staff = new Staff();
    static Person patient;

    static int staffid, patientID = 0;

    public static void main(String[] args) {
        appointmentSecretaryPart();
        appointmentPatientPart();
        appointmentAppointmentDatePart();
    }

    //list all Patients
    public static void listAllSecretaries() {

        System.out.println("************* LIST OF ALL SECRETARIES *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s", "NO", "STAFF ID", "SECRETARY NAME");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Staff secretary : IoC.dataLayer.getAllSecretaries()) {
            System.out.format("%5d %10s %20s ",
                    i, secretary.getStaff_id(), secretary.getName());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");

    }

//Staff Section
    private static void appointmentSecretaryPart() {
        //initializations
        String patientPhone = "";
        //Get list of all secretary IDs
        IDsList = IoC.dataLayer.getAllSecretaryIDs();
        //Show List of Secretaries
        listAllSecretaries();

        //Ensures an ID is entered.
        do {
            System.out.println("\nFrom the above list,\nEnter your valid Staff ID :  ");
            System.out.print("> ");
            staffid = input.nextInt();
        } while (staffid == 0);
        //Validate staff id
        if (IDsList.contains(staffid)) {
            staff = IoC.dataLayer.getSingleStaffUsingStaffID(staffid);
            System.out.println("Welcome " + staff.getName());
            //check if staff isn't a secretary
            /* if (staff.getDepartment_id()!=1) {
                System.out.println("You are not a Secretary");
                
            }*/
            //clear ArrayList
            IDsList.clear();
        } else {
            //Check if entry is a staff but not Secretary
            if (IoC.dataLayer.getAllStaffIDs().contains(staffid)) {
                System.out.println(staffid + " is not a Secretary ID");
            } else {
                System.out.println("-Staff ID does not exist!");
            }
            //return to top
            appointmentSecretaryPart();
        }
        appointment.setAllocated_by_staff_id(staffid);

    }

    //Patient Section
    private static void appointmentPatientPart() {

        //List all Patients
        listAllPatients();

        //Ensures a Patient ID is entered.
        do {
            System.out.println("\nFrom the above list,\nEnter the Patient's ID you want to allocate: ");
              System.out.print("> ");
            patientID = input.nextInt();
        } while (patientID == 0);

        //Validate Patient ID
        IDsList = IoC.dataLayer.getAllPatientsID();

        if (IDsList.contains(patientID)) {
            //Not needed to show patient details
            /* System.out.println("Patient Details for Patient ID " + patientID + ":");
            patient = dataLayer.getSinglePatientUsingPatientID(patientID);
            System.out.println("" + patient);*/
            appointment.setPatient_id(patientID);

        } else {
            System.out.println("\n-PATIENT ID DOES NOT EXIST!");
            //Navigate back to top
            appointmentPatientPart();
        }

    }

    //list all Patients
    public static void listAllPatients() {

        System.out.println("************* LIST OF ALL PATIENTS *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s", "NO", "PATIENT ID", "PATIENT NAME");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Person patient2 : IoC.dataLayer.getAllPatients()) {
            System.out.format("%5d %10s %20s ",
                    i, patient2.getPerson_id(), patient2.getName());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");
    }

    private static void appointmentAppointmentDatePart() {
        //initializations
        String appointmentDate, appointmentTime = "";

        //Ensures a Date is entered.
        do {
            System.out.println("\nKindly Enter Date in the format: DD-MM-YYYY eg.21-11-2016");
              System.out.print("> ");
            appointmentDate = input.next();
        } while (appointmentDate.isEmpty() || appointmentDate.length() != 10);

        //List all appointments with that Date --------------no need for this from Dr Xheng
        //-------------------------Dont forget Merge Tables in dataLayer query
        /* allAppointmentsArrayList= dataLayer.getAllAppointmentsUsingDate(appointmentDate);
        //Check for no booked appointments
         if (allAppointmentsArrayList.isEmpty()) {
             System.out.println("NO APPOINTMENTS BOOKED FOR "+appointmentDate);
         } else {
             int i=1;
               System.out.println("------------- APPOINTMENT LIST FOR "+appointmentDate+"---------------\n");
         for (Appointment appointment1 : allAppointmentsArrayList) {
                        System.out.println("APPOINTMENT "+i+".\nAppointment Date : "+appointment1.getDate()+
                     "\nAppointment Time : "+appointment1.getTime()+ 
                     "\nAllocated to Staff : "+appointment1.getAllocated_staff_id() + 
                     "\nPatient : "+appointment1.getPatient_id()+"\n"
             );             
             i++;
         }
         }*/
        //Confirm Date --------------no need for this from Dr Xheng
        /* System.out.println("\nDo you want to confirm the Date? (Type Yes or No): ");
        while (!confirm.equals("yes") && !confirm.equals("no")){
                confirm = input.nextLine();
            confirm = confirm.toLowerCase();
            //handle choices
            if (confirm.equals("yes")) {
                //set date to Object
         appointment.setDate(appointmentDate);
                //Now enquire abt appointment time by calling time enquiry method
                appointmentAppointmentTimePart(appointment.getDate());
            }
            
             else if (confirm.equals("no")) {
               //Navigate back to Top
                appointmentAppointmentDatePart();
            } else {
                System.out.println("Enter a valid response:");
            }
       
        
     }*/
        //set date to Object
        appointment.setDate(appointmentDate);
        //Now enquire abt appointment time by calling time enquiry method
        appointmentAppointmentTimePart(appointment.getDate());

    }

    private static void appointmentAppointmentTimePart(String appointmentDate) {
        String appointmentTime = "";
        int allocated_staff_id, treatment_id = 0;
        //Ensure Time is entered
        System.out.println("\nEnter Appointment time in the 24 hr format: eg.21:30 ");
         appointmentTime = input.nextLine();
        while (!appointmentTime.contains(":") || appointmentTime.length() != 5) {
            System.out.println("-Kindly enter valid time: eg. 21:30");
              System.out.print("> ");
            appointmentTime = input.nextLine();
        }
        //set Time to Object
        appointment.setTime(appointmentTime);
        //list all physio IDs
        allPhysioIDsList = IoC.dataLayer.getAllPhysioIDs();
        if (allPhysioIDsList.isEmpty()) {
            System.out.println("There are so Physiotherapists saved in the system");
            //go back to Secretary Page
            Secretary_Page.main(args);
        } else {
            //clear list before use
            IDsList.clear();
            System.out.println("\n******AVAILABLE PHYSIOTHERAPISTS FOR SAID DATE AND TIME******\n");
            System.out.println("--------------------------------------------------");
            //All physio IDs that are not in the booked IDs are available physios
            for (int physioID : allPhysioIDsList) {
                //if bookedIDList contains physioID then he is booked
                if (IoC.dataLayer
                        .getAllBookedPhysios_IdsUsingDateandTime(appointmentDate, appointmentTime)
                        .contains(physioID)) {
                    //he is booked                      
                } //else he is available
                else {
                    //get physio obj
                    // IoC.dataLayer.getSingleStaffUsingStaffID(physioID); //returns Physio Staff
                    System.out.format("%20s %s %n", "Staff ID: ", IoC.dataLayer.getSingleStaffUsingStaffID(physioID).getStaff_id());
                    System.out.format("%20s %s %n", "Department: ", IoC.dataLayer.getSingleStaffUsingStaffID(physioID).getDepartment_name());
                    System.out.format("%20s %s %n", "Staff Name: ", IoC.dataLayer.getSingleStaffUsingStaffID(physioID).getName());
                    System.out.format("%20s %s %n", "Phone Number: ", IoC.dataLayer.getSingleStaffUsingStaffID(physioID).getPhone_number());
                    System.out.println("--------------------------------------------------");
                    // System.out.println(dataLayer.getSingleStaffUsingStaffID(physioID)); replaced with above
                    //add id to list of Available Physios
                    IDsList.add(physioID);
                }

            }

        }

        //if all physios are booked ie.IDsList is empty,choose another time
        if (IDsList.isEmpty()) {
            System.out.println("- Sorry all Physiotherapists are booked at this time\nChoose "
                    + "another appointment time");
            appointmentAppointmentTimePart(appointmentDate);
        } else {

            //Ensure that staff id is entered
            System.out.println("\nFrom the above list, \n"
                    + "Kindly enter ID of the Staff you want to assign to Patient "//+patient.getName()
            );
              System.out.print("> ");
            allocated_staff_id = input.nextInt();
            while (allocated_staff_id == 0) {
                System.out.println("-Kindly enter valid staff id ");
                  System.out.print("> ");
                allocated_staff_id = input.nextInt();
            }

            //check if staff_id listed is in available staff list
            if (IDsList.contains(allocated_staff_id)) {
                System.out.println("You assigned Staff id " + allocated_staff_id
                        + " to Patient: " + IoC.dataLayer
                                .getSinglePatientUsingPatientID(appointment.getPatient_id()).getName());

                appointment.setAllocated_staff_id(allocated_staff_id);

                //list all available treatments
                System.out.println("********LIST OF ALL TREATMENT SERVICES********");
                allTreatments = IoC.dataLayer.getAllTreatments();
                int i = 1;
                for (Treatment treatment : allTreatments) {
                    System.out.println(i + ". " + treatment.getName());
                    i++;
                }
                 System.out.println("--------------------------------------------------");
                System.out.println("\nFrom the list above,\nEnter Number corresponding to treatment required:");
                //ensure treatment is entered   
                  System.out.print("> ");
                treatment_id = input.nextInt();
                while (treatment_id == 0) {
                    System.out.println("-Kindly enter valid treatment number eg. 2 ");
                      System.out.print("> ");
                    treatment_id = input.nextInt();
                }
                //insert treatment name instead of treatment_id
                appointment.setTreatment_ids(IoC.dataLayer
                        .getSingleTreatmentusingTreatmentID(treatment_id).getName());

                ///end of booking addition
                //confirm and save booking method
                confirmSaveAppointment(appointment);

            } else {
                System.out.println("-The staff ID you entered is not available or does not exist");
                // navigate back to time
                appointmentAppointmentTimePart(appointmentDate);
            }
        }
    }

    public static void confirmSaveAppointment(Appointment appointment) {
        String confirm = "";
        System.out.println("\nDo you want to confirm the Appointment? (Type Yes or No): ");
        while (!confirm.equals("yes") && !confirm.equals("no")) {
              System.out.print("> ");
            confirm = input.nextLine();
            confirm = confirm.toLowerCase();
            //handle choices
            switch (confirm) {

                case "yes":
//set status to 1 ie.open
                    appointment.setStatus(1);
//save appointment    
                    boolean save = IoC.dataLayer.addAppointment(appointment);
                    if (save) {
                        System.out.println("-Appointment booking successful");
                        //Navigate back to Secretary Page
                        Secretary_Page.main(args);
                    } else {
                        System.out.println("-Apppointment booking unsuccessful");
                        //Navigate back to Secretary Page
                        Secretary_Page.main(args);
                    }
                    break;
                case "no":
                    //Navigate back to Secretary Page
                    Secretary_Page.main(args);
                    break;
                default:
                    System.out.println("-Enter a valid response:");
                    break;
            }

        }

    }

}
