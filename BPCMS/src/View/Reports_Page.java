/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.Person;
import Model.Report;
import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
class Reports_Page {

    static Scanner input = new Scanner(System.in);
    private static String[] args;

    public static void main(String[] args) {
        //Enquire about Month
        monthEnquiry();

    }

    //Go Back Msg
    public static void monthEnquiry() {
        int confirm = 10;
        String selectedMonth,selectedMonthInFull="";
        System.out.println("\nWhich Month Reports do you want :\n"
                  + "0. Go Back\n"
                + "1. September\n"
                + "2. October\n"
                + "3. November\n"
                + "4. December\n"
                + "5. January\n"
                + "6. February\n"
             
        );
        while (confirm == 10) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            confirm = input.nextInt();
        }
        switch (confirm) {
            case 1:
                selectedMonthInFull="September".toUpperCase();
                selectedMonth="09";
                reportMethod(selectedMonth,selectedMonthInFull);
                break;

            case 2:
                  selectedMonthInFull="October".toUpperCase();
                selectedMonth="10";
                reportMethod(selectedMonth,selectedMonthInFull);
                break;

            case 3:
                  selectedMonthInFull="November".toUpperCase();
                selectedMonth="11";
                reportMethod(selectedMonth,selectedMonthInFull);
                break;

            case 4:
                  selectedMonthInFull="December".toUpperCase();
                selectedMonth="12";
                reportMethod(selectedMonth,selectedMonthInFull);
                break;

            case 5:
                  selectedMonthInFull="January".toUpperCase();
                selectedMonth="01";
                reportMethod(selectedMonth,selectedMonthInFull);
                break;
 case 6:
                  selectedMonthInFull="February".toUpperCase();
                selectedMonth="02";
                reportMethod(selectedMonth,selectedMonthInFull);
                break;

            case 0:
                //Navigate back to Home Page
                HomePage.main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                monthEnquiry();
                break;
        }

    }
    
    
    //list all appointments of each user -- no of appointments,attended,missed,cancelled,no of products ordered
    public static void reportMethod(String month,String selectedMonthInFull){
        if (month.isEmpty() || selectedMonthInFull.isEmpty()) {
            System.out.println("No Month Parameters");
        }
        
        else{ System.out.println("    *******************************   MONTH OF "+selectedMonthInFull+" REPORT    *******************************     ");
          System.out.println("------------------------------------------------------------"
                    + "----------------------------------------------------");
            //Show Total Number of Appointments for that Month
            System.out.format("%30s %s %n", "--------- TOTAL NUMBER OF APPOINTMENTS FOR "+selectedMonthInFull+" : ",
                    IoC.dataLayer.getTotalCountofPatientAppointmentsUsingDate(month)+" ---------");
            System.out.println();

            for (Person patient2 : IoC.dataLayer.getAllPatients()) {
                System.out.format("%20s %s %n", "Patient ID: ", patient2.getPerson_id());
                System.out.format("%20s %s %n", "Patient Name: ", patient2.getName());
                //i++;
                System.out.println();
                System.out.printf("%20s %25s %15s %15s %20s", "NO OF APPOINTMENTS", "ATTENDED/CHECKED IN", "MISSED",
                    "CANCELLED","ORDERED PRODUCTS");
            System.out.println();
            
             //Get Reports Using Patient ID and Month

            for (Report report : IoC.dataLayer.getCountsofPatientAppointmentsAndProductsUsingPatientIDandDate(patient2.getPerson_id(),month)) {
                System.out.format("%20s %25s %15s %15s %20s",
                        report.getNo_of_appointments(), report.getNo_of_attended(),report.getNo_of_missed(),
                        report.getNo_of_cancelled(),report.getNo_of_products_ordered());
System.out.println();
                System.out.println();
            }
       System.out.println("------------------------------------------------------------"
                  + "----------------------------------------------------");
            
        }
        //go back to monthEnquiry
        monthEnquiry();
        }
    }
    
    
    
    
}
