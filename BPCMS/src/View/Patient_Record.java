/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.DetailedAppointment;
import Model.Person;
import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
public class Patient_Record {

    static Scanner input = new Scanner(System.in);
    static Person patient;
    private static String[] args;

    public static void main(String[] args) {
        //Use this format to show Output
        /*System.out.format("%16s%16s%16s\n", "First Name","Last Name","Products");
        System.out.format("%16s%16s%16d\n", "Mak","Ali",12);
        System.out.format("%16s%16s%16d\n", "Mazhasdhkjashd","Alasdi",12);*/
        
           //Call list of all patients
        listAllPatients();
        int patient_id  = patientIdEnquiry();

        patient = IoC.dataLayer.getSinglePatientUsingPatientID(patient_id);
        //check for match
        if (patient.isEmpty(patient)) {
            System.out.println("-No Such Patient Name as " + patient.getName().toUpperCase());
            //navigate to top
            main(args);
        } else {
            System.out.println("\n-Welcome " + patient.getName());
            System.out.println("*************** PATIENT DETAILS ******************");
            System.out.println("--------------------------------------------------");
            System.out.format("%20s %s %n", "Patient ID: ", patient.getPerson_id());
            System.out.format("%20s %s %n", "Patient Name: ", patient.getName());
            System.out.format("%20s %s %n", "Phone Number: ", patient.getPhone_number());
            System.out.println("--------------------------------------------------\n");

             System.out.println("********************************* RECORD OF PATIENT APPOINTMENT HISTORY ************************************");
            System.out.println("-----------------------------------------------------------------------"
                    + "--------------------------------------------------------------------------------");
            
                 //check for empty List
        if (IoC.dataLayer.getPatientDetailedAppointmentsUsingPatientID(patient.getPerson_id()).isEmpty()) {
            System.out.println("-No Booked Appointments");
            System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
             //call confirm method
           confirmMsg();
        }
        else{
            System.out.printf("%5s %20s %20s %20s %20s %20s %20s %20s", "NO", "APPOINTMENT DATE", "APPOINTMENT TIME", "PHYSIOTHERAPIST", "TREATMENT","PRODUCTS", "STATUS","CHECK IN STATUS");
            System.out.println();
           System.out.println("-----------------------------------------------------------------------"
                    + "--------------------------------------------------------------------------------");
            int i = 1;
            for (DetailedAppointment appointment : IoC.dataLayer.getPatientDetailedAppointmentsUsingPatientID(patient.getPerson_id())) {
                System.out.format("%5d %20s %20s %20s %20s %20s %20s %20s",
                        i, appointment.getDate(), appointment.getTime(), appointment.getStaff_name(), appointment.getTreatments(),appointment.getPrescriptions(), appointment.getStatus(),appointment.getCheckin_status());
                i++;
                System.out.println();
            }
         System.out.println("-----------------------------------------------------------------------"
                    + "--------------------------------------------------------------------------------");  
           //call confirm method
           confirmMsg();
        }
        }

    }

       //list all Patients
    public static void listAllPatients() {

        System.out.println("************* LIST OF ALL PATIENTS *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s", "NO", "PATIENT ID", "PATIENT NAME");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Person patient2 : IoC.dataLayer.getAllPatients()) {
            System.out.format("%5d %10s %20s ",
                    i, patient2.getPerson_id(), patient2.getName());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");
    }
    
    //Patient ID Enquiry
    public static int patientIdEnquiry() {
        int IDinput = 0;
        System.out.println("\nFrom the above list,\nKindly Enter your Patient ID");
        while (IDinput==0) {
            System.out.println("-Kindly enter valid ID");
              System.out.print("> ");
            IDinput = input.nextInt();
        }
        return IDinput;
    }
       //Confirm Method
    public static void confirmMsg() {
        int confirm = 10;

        System.out.println("\nDo you want :\n"
                + "0. Go Back to Main Patient Page\n"
                + "1. Restart View Patient History Process"
                );
        while (confirm == 10) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            confirm = input.nextInt();
        }
        switch (confirm) {
            case 0:
                //Navigate back to Patient Page
                Patient_Page.main(args);
                break;
            case 1:
                //Navigate back to Top
                main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                confirmMsg();
                break;
        }

    }

}
