/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.DetailedAppointment;
import Model.Person;
import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
public class ShopKeeper_Page {
     static Scanner input = new Scanner(System.in);
    private static String[] args;
    
    
    
    
    public static void main(String[] args) {
        //list all prescriptions for each patient
        listAllPrescriptionsOfEachPatient();
        //go Back msg
        goBackMsg();
        
        
    }
    
    //List All Products of each Patient
    public static void listAllPrescriptionsOfEachPatient() {

        System.out.println("******** LIST OF ALL PRESCRIPTION PRODUCTS FOR EACH PATIENT ********");
        System.out.println("---------------------------------------------------------");

       // int i = 1;
        for (Person patient2 : IoC.dataLayer.getAllPatients()) {
            System.out.format("%20s %s %n", "Patient ID: ", patient2.getPerson_id());
            System.out.format("%20s %s %n", "Patient Name: ", patient2.getName());
            //i++;
            System.out.println();
            //check for when Patients Has not been given any prescription
            if ( IoC.dataLayer.getPatientDetailedAppointmentsUsingPatientID(patient2.getPerson_id()).isEmpty()) {
                System.out.println("-No Prescription Products for Patient :"+patient2.getName());
                 System.out.println("---------------------------------------------------------");
            }
            else{
            System.out.printf("%20s %20s", "PRODUCT NAME", "REQUESTED DATE");
            System.out.println();
            //Get All Patient Appointments Using Patient ID

            for (DetailedAppointment appointment : IoC.dataLayer.
                    getPatientDetailedAppointmentsUsingPatientID(patient2.getPerson_id())) {
                //check for null objects
                if ( appointment.getPrescriptions()==null) {
                    
                }else{
                System.out.format("%20s %20s ",
                        appointment.getPrescriptions(), appointment.getDate());

                System.out.println();}
            }
            System.out.println("---------------------------------------------------------");
        }
        }
    }
    
    
     //Go Back Msg
    public static void goBackMsg() {
        int confirm = 10;

        System.out.println("\nDo you want :\n"
                + "0. Go Back to Home Page");
        while (confirm == 10) {
            System.out.println("-Kindly enter valid selection eg. 0");
              System.out.print("> ");
            confirm = input.nextInt();
        }
        switch (confirm) {
            case 0:
                //Navigate back to Home Page
                HomePage.main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                goBackMsg();
                break;
        }

    }
    
}
