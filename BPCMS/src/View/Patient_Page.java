/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.Scanner;

/**
 *
 * @author AFRIFA
 */
public class Patient_Page {
     //Constructor
    public Patient_Page() {
    }
    
 
     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int user_input = 10;
        
         //print Welcome Msg
       welcomeMessageForPatient();
      //Ensure User enters preferred functionality
        System.out.println("\nEnter your Choice: ");
          System.out.print("> ");
         user_input = input.nextInt();

            //Validating Input
            while (user_input < 0 || user_input > 2) {
                System.out.println("Enter a valid number: ");
                  System.out.print("> ");
                user_input = input.nextInt();
            }
            //Call choice method
            userChoiceForPatient(user_input);
                       
              //VIEW PATIENT DETAILS AND APPOINTMENTS HISTORY RECORD 
           if(user_input==1){
             //Navigate to Patient_Record
             Patient_Record.main(args);
           }
           
              //VIEW PATIENT CHECKIN 
           if(user_input==2){
             //Navigate to Patient_Record
             Patient_CheckIn.main(args);
           }
           
      

     }
     
     
     
      
     //Welcome Message 
       public static void welcomeMessageForPatient(){
        System.out.println("\nChoose what function you want to perform"
                  + "\n0. Go Back"       
                + "\n1. View Patient Record"
                + "\n2. Check In into Appointment"           
                 
               
               );
    }
       
       //user Choice 
        public static String userChoiceForPatient(int input){
        switch (input) {
          
            case 1:
                System.out.println("\n---- View Patient Record and Appointment History ------");
                break;
              case 2:              
                 System.out.println("\n---- Check In Patient Page  ------");
                break;
                 case 0:              
                System.out.println("-Exited ");
                    String[] args = new String[10];
                //Navigate back to Home Page
                HomePage.main(args);
                break;
            default:
                System.out.println("\nInvalid Choice");
                welcomeMessageForPatient();
                break;
        }
        return "";
        }
       
}
