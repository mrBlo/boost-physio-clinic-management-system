/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.Person;
import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
public class AddPatient_Page {

    static Person patient = new Person();
     static String[] args = new String[10];
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        patientDetailsInput();

    }

  
    //ENTERING AND VALIDATING PATIENT DETAILS
    private static Person patientDetailsInput() {
        //initializations
        String patientName, patientPhone, confirm = "";
        int patientID = 0;
  //Ensures a Name is entered.
        do{
        System.out.println("Enter Patient's Name: ");
          System.out.print("> ");
        patientName = input.nextLine();
        }
        while (patientName.isEmpty()); 
        patient.setName(patientName);
        //Phone Number
        System.out.println("Enter Patient's Phone Number: ");
          System.out.print("> ");
        patientPhone = input.nextLine();

        //Validating Phone
        while (patientPhone.length() != 11) {
            System.out.println("Enter an 11 digit phone number: ");
              System.out.print("> ");
            patientPhone = input.nextLine();
        }
        patient.setPhone_number(patientPhone);

        /*-----------------------------------Do Check if patient is already saved--------------------------------------------------------*/
        do {
            System.out.println("Are you to save? (Type Yes or No): ");
              System.out.print("> ");
            confirm = input.nextLine();
            confirm = confirm.toLowerCase();
            //handle choices
            if (confirm.equals("yes")) {
                //Save Patient Query here 
              boolean status= IoC.dataLayer.addPatient(patient);
                if (status) {
                    System.out.println("-Patient Was Saved");
                } else {
                     System.out.println("-Failed to Save Patient");
                }
                //Navigate back to Secretary Page
                Secretary_Page.main(args);

               
            } else if (confirm.equals("no")) {
                              //Navigate back to Secretary Page
                Secretary_Page.main(args);

            } else {
                System.out.println("Enter a valid response:");
            }

        } while (!confirm.equals("yes") && !confirm.equals("no"));

        return patient;
    }
    
}
