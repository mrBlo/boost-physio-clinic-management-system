/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.Person;
import static View.AddPatient_Page.args;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
public class ViewPatient_Page {

    static ArrayList<Person> arrayList;
    static ArrayList<Integer> patientIDsList;
    static Scanner input = new Scanner(System.in);
    static String confirm = "";
    static int patientID;
    static int user_input = 0;
    static Person patient;
    private static String patientName;
    private static String patientPhone;
    private static boolean status;

    public static void main(String[] args) {
        //show list of all patients
        listAllPatients();
        System.out.println("\nDo you want to edit? (Type Yes or No): ");
        do {
              System.out.print("> ");
            confirm = input.nextLine();
            confirm = confirm.toLowerCase();
            //handle choices
            if (confirm.equals("yes")) {
                System.out.println("\nFrom the above list,\nEnter Patient ID you want to edit : ");
                  System.out.print("> ");
                patientID = input.nextInt();
                patientIDsList = IoC.dataLayer.getAllPatientsID();

                if (patientIDsList.contains(patientID)) {
                    System.out.println("Patient Details for Patient ID " + patientID + ":");
                    patient = IoC.dataLayer.getSinglePatientUsingPatientID(patientID);
                    displaySelectedPatient(patient);
                    //call Edit method
                    editMethod();

                } else {
                    System.out.println("-Patient ID does not exist!");
                    //Navigate back to top
                    main(args);
                }

            } else if (confirm.equals("no")) {
                //Navigate back to Secretary Page
                Secretary_Page.main(args);

            } else {
                System.out.println("Enter a valid response:");
            }
        } while (!confirm.equals("yes") && !confirm.equals("no"));

    }

    //list all Patients
    public static void listAllPatients() {

        System.out.println("************* LIST OF ALL PATIENTS *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s %20s", "NO", "PATIENT ID", "PATIENT NAME", "PHONE NUMBER");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Person patient2 : IoC.dataLayer.getAllPatients()) {
            System.out.format("%5d %10s %20s %20s",
                    i, patient2.getPerson_id(), patient2.getName(), patient2.getPhone_number());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");
    }

    //display selected Patient
    public static void displaySelectedPatient(Person patient) {
        if (patient==null) {
            System.out.println("Empty Patient Object");
        }else{
        System.out.println("*************** PATIENT DETAILS ******************");
        System.out.println("--------------------------------------------------");
        System.out.format("%20s %s %n", "Patient ID: ", patient.getPerson_id());
        System.out.format("%20s %s %n", "Patient Name: ", patient.getName());
        System.out.format("%20s %s %n", "Phone Number: ", patient.getPhone_number());
        System.out.println("--------------------------------------------------\n");
        }
    }

    //Print askEditQuestion
    public static void editMethod() {
        System.out.println("Enter preferred corresponding number,\nDo you want to"
               + "\n0.Go Back "
                + "\n1.Edit Name"
                + "\n2.Edit Phone Number");
          System.out.print("> ");
        user_input = input.nextInt();
        while (user_input < 0 || user_input > 2) {
            System.out.println("Please enter a valid number between 1 and 3: ");
              System.out.print("> ");
            user_input = input.nextInt();
        }
        //handling user inputs
        switch (user_input) {
            case 1:
                do {
                    System.out.println("Enter Patient's Name: ");
                      System.out.print("> ");
                    patientName = input.nextLine();
                } while (patientName.isEmpty());
                //patient.setName(patientName);
                //update call here
                status = IoC.dataLayer.updatePatientNameUsingID(patientID, patientName);
                if (status) {
                    System.out.println("-Patient Name Updated!");
                } else {
                    System.out.println("-Name Not Updated!");
                }
                //Navigate back to top
               main(args);
                break;

            case 2:
                System.out.println("Enter Patient's Phone Number: ");                
                patientPhone = input.nextLine();
                //Validating Phone
                while (patientPhone.length() != 11) {
                    System.out.println("Enter an 11 digit phone number: ");
                      System.out.print("> ");
                    patientPhone = input.nextLine();
                }
                // patient.setPhone_number(patientPhone);
                //update call here
                status = IoC.dataLayer.updatePatientPhoneUsingID(patientID, patientPhone);
                if (status) {
                    System.out.println("-Patient Phone Updated!");
                } else {
                    System.out.println("-Phone Not Updated!");
                }
                 //Navigate back to top
               main(args);
                break;

            case 0:
                //Navigate back to Secretary Page
                Secretary_Page.main(args);
                break;
            default:
                System.out.println("Enter a valid response:");
                break;
        }

    }
}
