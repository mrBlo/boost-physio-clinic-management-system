/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.util.Scanner;

/**
 *
 * @author ia16abg
 */
public class HomePage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int user_input = 10;

        //Initializations
        welcomeMessage();
        //Ensure User enters User Type
        System.out.println("Enter your Choice: ");
        try {
              System.out.print("> ");
            user_input = input.nextInt();

            //Validating Input
            while (user_input < 0 || user_input > 5) {
                System.out.println("Enter a valid number: ");
                  System.out.print("> ");
                user_input = input.nextInt();
            }
            //Call userChoice method
            userChoice(user_input);
            
            /*-------- handling choices-------------*/
            
            //SECRETARY
           if(user_input==1){
               //Navigate to Secretary Page
                 Secretary_Page.main(args);
           
           }
           
             //PATIENT
           if(user_input==2){
              //Navigate to Secretary Page
                 Patient_Page.main(args);
        
           }
              //PHSYIOTHERAPIST
           if(user_input==3){
           //Navigate to Physio Page
                 Physio_Page.main(args);
        
           }
              //STORE KEEPER
           if(user_input==4){
            //Navigate to  ShopKeeper Page
                 ShopKeeper_Page.main(args);
           }
              //REPORTS
           if(user_input==5){
            //Navigate to  Manager Page
                Reports_Page.main(args);
        
           }
            

        } catch (Exception e) {
            //WORK ON THIS LATER
            System.out.println("-You entered a String\nShutting down system");
        }

    }

    public static void welcomeMessage() {
        System.out.println("\n----------------------------------- WELCOME TO BOOST PHSYIO CLINIC -----------------------------------"
                + "\n***USERS SECTION***"
                + "\nChoose which user you are \n"
                + "1. Secretary"
                + "\n2. Patient"
                  + "\n3. Physiotherapist"
                + "\n4. Shop Keeper"
                + "\n*** REPORTS SECTION ***"
                + "\n5. Print Monthly Reports"
         + "\n0. Exit ");
        System.out.println("-------------------------------------------------------------------------------------------------");
    }

    public static String userChoice(int input) {
        switch (input) {
            case 1:
                System.out.println("\n---- SECRETARY PAGE ------");
                                break;
            case 2:
                System.out.println("\n---- PATIENT PAGE ------");
                break;
            case 3:
                System.out.println("\n---- PHYSIOTHERAPIST PAGE ------");
                break;
            case 4:
               System.out.println("\n---- SHOP KEEPER PAGE ------");
                break;
                  case 5:
                  System.out.println("\n---- MONTHLY REPORTS PAGE ------");
              
                break;
                 case 0:
                System.out.println("\n------------------------ BYE USER! ------------------------------");
                System.exit(0);
                break;
            default:
                System.out.println("\nInvalid Choice");
                break;
        }
        return "";
    }

}
