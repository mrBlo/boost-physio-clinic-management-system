/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.IoC;
import Model.DetailedAppointment;
import Model.Person;
import Model.Prescription;
import Model.Staff;
import java.util.Scanner;

/**
 *
 * @author AFRIFA
 */
class Physio_Page {

    static Scanner input = new Scanner(System.in);
    static Person patient;
    static Staff physio;
    static DetailedAppointment appointment;
    private static String[] args;

    public static void main(String[] args) {
        //list all physios
        listAllPhysios();
        //get selected Physio 
        physio = staffIDEnquiry();
        System.out.println("\n-Welcome " + physio.getName());
        //list all appointments of physio
        listAllAppointments(physio);
        //get Selected appointment
        appointment = appointmentIDEnquiry();
        //show selected appointment
        displaySelectedAppointment(appointment);
       // System.out.println("--" + appointment); replaced by display above
        
        //Selection Enquiry
        int choice=selectionMsgForPhysio();
        userChoice(choice);
        

    }
    
   
//list all physios
    public static void listAllPhysios() {

        System.out.println("************* LIST OF ALL PHYSIOTHERAPISTS *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s", "NO", "STAFF ID", "STAFF NAME");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Staff staff : IoC.dataLayer.getAllPhysios()) {
            System.out.format("%5d %10s %20s ",
                    i, staff.getStaff_id(), staff.getName());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");

    }

    public static Staff staffIDEnquiry() {
        int id = 0;
        do {
            System.out.println("\nFrom the above list of Physiotherapists,\n"
                    + "Kindly Enter your Staff ID :");
              System.out.print("> ");
            id = input.nextInt();
        } while (id == 0);
        if (!IoC.dataLayer.getAllPhysioIDs().contains(id)) {
            System.out.println("-Enter a valid Staff ID");
            //navigate back to top
            staffIDEnquiry();

        } else {
            //get Staff using ID
            physio = IoC.dataLayer.getSingleStaffUsingStaffID(id);
        }
        //return staff
        return physio;
    }

    //list all Patients
    public static void listAllAppointments(Staff physio) {

        System.out.println("********************************************************************"
                + "* LIST OF ALL APPOINTMENTS ASSIGNED TO " + physio.getName() + " "
                + "********************************************************************");
        System.out.println("-----------------------------------------------------------------------------"
                + "--------------------------------------------------------------------------------------");
        System.out.printf("%5s %15s %20s %20s %20s %20s %20s %10s %20s %20s %30s",
                "NO", "APPOINTMENT ID", "PATIENT NAME", "APPOINTMENT DATE", "APPOINTMENT TIME", "APPOINTED BY",
                "TREATMENT", "STATUS", "CHECK IN STATUS", "PRODUCTS", "NOTES/COMMENTS");
        System.out.println();
        System.out.println("-----------------------------------------------------------------------------"
                + "--------------------------------------------------------------------------------------");

        int i = 1;
        for (DetailedAppointment appointment2 : IoC.dataLayer.getAllDetailedAppointmentsUsingStaffID(physio.getStaff_id())) {
            System.out.format("%5d %15s %20s %20s %20s %20s %20s %10s %20s %20s %30s",
                    i, appointment2.getId(), appointment2.getPatient_name(), appointment2.getDate(),
                    appointment2.getTime(), appointment2.getAllocated_by_staff_name(), appointment2.getTreatments(),
                    appointment2.getStatus(), appointment2.getCheckin_status(), appointment2.getPrescriptions(),
                    appointment2.getComments());
            i++;
            System.out.println();
        }
        System.out.println("-----------------------------------------------------------------------------"
                + "--------------------------------------------------------------------------------------");
    }
    
    
      //show Selected Appointment
     public static void displaySelectedAppointment(DetailedAppointment appointment){
       System.out.println("******************************************** SELECTED APPOINTMENT ****************************************************");
            System.out.println("---------------------------------------------------------------------------------------------------"
                    + "------------------------------------------------------------------------------------------------");
            System.out.printf("%10s %20s %20s %20s %10s %25s %20s %20s %20s %20s %25s",
                   "APPOINTMENT ID","PATIENT NAME","PHYSIOTHERAPIST","APPOINTMENT DATE", "TIME",
                    "ASSIGNED BY", "CHECK IN STATUS", "STATUS","NOTES", "PRODUCTS", "TREATMENT");
            System.out.println();
             System.out.println("----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");
            System.out.format("%11d %20s %20s %20s %15s %25s %20s %20s %20s %20s %25s",
                        appointment.getId(),appointment.getPatient_name(),appointment.getStaff_name(),
                        appointment.getDate(), appointment.getTime(), appointment.getAllocated_by_staff_name(),
                        appointment.getCheckin_status(),appointment.getStatus(),appointment.getComments(),
                       appointment.getPrescriptions(), appointment.getTreatments() );
                System.out.println("\n----------------------------------------------------------------------------------------------------------------"
                    + "---------------------------------------------------------------------------------");  
     }
  

    //appointment ID Enquiry
    public static DetailedAppointment appointmentIDEnquiry() {
        int selection_id = 0;
        do {
            System.out.println("\nFrom the above list of Appointments,\n"
                    + "Kindly Enter Appointment ID you want to work on :");
              System.out.print("> ");
            selection_id = input.nextInt();
        } while (selection_id == 0);
        //check for valid physio's appointment
        if (!IoC.dataLayer.getAllAppointmentIDsofSinglePhysioUsingStaffID(physio.getStaff_id()).contains(selection_id)) {
            System.out.println("-Enter a valid Appointment ID from list");
            //navigate back to top
            appointmentIDEnquiry();
        } else {
            //get Appointment using Appointment ID
            appointment = IoC.dataLayer.getSingleDetailedAppointmentsUsingID(selection_id);
        }
        //return staff
        return appointment;
    }

     public static void myMethod(){
     int choice=selectionMsgForPhysio();
        userChoice(choice);
        }
    
    //Selection msg 
       public static int selectionMsgForPhysio(){
                        int user_input=10;
        System.out.println("\nChoose what function you want to perform "
                 + "\n0. Go Back"   
                + "\n1. Add Notes/Comments"
                + "\n2. Add Prescription Products"
                    
               );
            //Ensure User enters preferred functionality
              System.out.print("> ");
             user_input = input.nextInt();
            //Validating Input
            while (user_input < 0 || user_input > 2) {
                System.out.println("Enter a valid number: ");
                  System.out.print("> ");
                user_input = input.nextInt();
            }
            return user_input;
    }
    
         //user Choice 
        public static String userChoice(int input){
               switch (input) {
            case 1:
                notesedit();
                break;
            case 2:
               //list all prescriptions
                listAllPrescriptions();
                prescriptionSelection();
                break;
              case 0:              
                System.out.println("\n-Exited ");
                                //Navigate back to Home Page
                HomePage.main(args);
                break;
            default:
                System.out.println("\nInvalid Choice");
                //Navigate to Top
                myMethod();
                break;
        }
        return "";
        }
       
    
        
            //Notes/Comments edit
    public static void notesedit() {
        String notes = "";
          System.out.println("\nKindly Enter Your Notes:");
            System.out.print("> ");
            notes = input.nextLine();        
        //Ensures a note is entered.
        while (notes.isEmpty()){
           System.out.println("-Kindly Enter valid Notes:");
             System.out.print("> ");
            notes = input.nextLine();        
        }
        /*do {
            System.out.println("\nKindly Enter Your Notes:");
            notes = input.nextLine();
        } while (notes.isEmpty());*/
       
        //update query here //  update Appointment Notes using Id
        boolean update = IoC.dataLayer.updateAppointmentNotesUsingID(appointment.getId(), notes);
        if (update) {
             appointment =  IoC.dataLayer.getSingleDetailedAppointmentsUsingID(appointment.getId());
          System.out.println("*************** UPDATED APPOINTMENT DETAILS ******************");
            System.out.println("----------------------------------------------------");
            System.out.format("%20s %s %n", "Appointment ID: ", appointment.getId());
            System.out.format("%20s %s %n", "Appointment Date: ", appointment.getDate());
            System.out.format("%20s %s %n", "Appointment Time: ", appointment.getTime());
             System.out.format("%20s %s %n", "Staff Name: ",  appointment.getStaff_name());
            System.out.format("%20s %s %n", "Treatment: ", appointment.getTreatments());
            System.out.format("%20s %s %n", "Status: ", appointment.getStatus());
             System.out.format("%20s %s %n", "Check In Status: ", appointment.getCheckin_status());
             System.out.format("%20s %s %n", "Notes/Comments: ", appointment.getComments());
                        
            System.out.println("----------------------------------------------------");
            System.out.println("-Appointment Notes Updated");
            confirmMsg();
        } else {
            System.out.println("-Appointment Notes Not Updated");
            confirmMsg();
        }
    }
    
    
    
      //prescription edit
   public static void prescriptionSelection() {
       int selection_id=0;
       Prescription prescription;
        do {
            System.out.println("\nFrom the above list of Prescription Products,\n"
                    + "Kindly Enter ID of the Prescription Product you want to prescribe :");
              System.out.print("> ");
            selection_id = input.nextInt();
        } while (selection_id == 0);
       
           if (!IoC.dataLayer.getAllPrescriptionIDs().contains(selection_id)) {
            System.out.println("-Enter a valid Prescription Product ID from list");
            //navigate back to top
            prescriptionSelection();
        } else {
               //get Prescription using id
               prescription= IoC.dataLayer.getSinglePrescriptionusingPrescriptionID(selection_id);  
            //update query here //update Appointment Prescription using Id and prescription name
             boolean update = IoC.dataLayer.updateAppointmentPrescriptionUsingID(appointment.getId(),prescription.getName());
         if (update) {
              appointment =  IoC.dataLayer.getSingleDetailedAppointmentsUsingID(appointment.getId());
          System.out.println("*************** UPDATED APPOINTMENT DETAILS ******************");
            System.out.println("----------------------------------------------------");
            System.out.format("%20s %s %n", "Appointment ID: ", appointment.getId());
            System.out.format("%20s %s %n", "Appointment Date: ", appointment.getDate());
            System.out.format("%20s %s %n", "Appointment Time: ", appointment.getTime());
             System.out.format("%20s %s %n", "Staff Name: ",  appointment.getStaff_name());
            System.out.format("%20s %s %n", "Treatment: ", appointment.getTreatments());
            System.out.format("%20s %s %n", "Status: ", appointment.getStatus());
             System.out.format("%20s %s %n", "Check In Status: ", appointment.getCheckin_status());
             System.out.format("%20s %s %n", "Prescription Product: ", appointment.getPrescriptions());
                        
            System.out.println("----------------------------------------------------");
             
            System.out.println("-Prescription Product Updated");
            confirmMsg();
        } else {
            System.out.println("-Prescription Product Not Updated");
            confirmMsg();
        }
           
           }
       
    }
    
    //list all prescriptions
     public static void listAllPrescriptions() {

        System.out.println("************* LIST OF ALL PRESCRIPTION PRODUCTS *****************");
        System.out.println("---------------------------------------------------------");
        System.out.printf("%5s %10s %20s", "NO", "ID", "PRODUCTS");
        System.out.println();
        System.out.println("---------------------------------------------------------");

        int i = 1;
        for (Prescription prescription : IoC.dataLayer.getAllPrescriptions()) {
            System.out.format("%5d %10s %20s ",
                    i, prescription.getId(), prescription.getName());
            i++;
            System.out.println();
        }
        System.out.println("---------------------------------------------------------");

    }
   
    
     //Confirm Method
    public static void confirmMsg() {
        int confirm = 10;

        System.out.println("\nDo you want :\n"
                + "0. Go Back to Home Page\n"
                + "1. Perform another Physio operation");
        while (confirm == 10) {
            System.out.println("-Kindly enter valid selection eg. 1");
              System.out.print("> ");
            confirm = input.nextInt();
        }
        switch (confirm) {
            case 0:
                //Navigate back to Home Page
                HomePage.main(args);
                break;
            case 1:
                //Navigate back to selection msg method
                //myMethod();
                main(args);
                break;
            default:
                System.out.println("-Invalid Selection");
                confirmMsg();
                break;
        }

    }
    
}
