/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author AFRIFA
 */
public class DetailedAppointment {
       private int id; 
    private String patient_name,staff_name,allocated_by_staff_name,
            status,checkin_status,date,time,comments,prescriptions,treatments;
      
       
       //Constructors
    public DetailedAppointment() {
    }
//with id
    public DetailedAppointment(int id, String patient_name, String staff_name, String allocated_by_staff_name, String status, String checkin_status, String date, String time, String comments, String prescriptions, String treatments) {
        this.id = id;
        this.patient_name = patient_name;
        this.staff_name = staff_name;
        this.allocated_by_staff_name = allocated_by_staff_name;
        this.status = status;
        this.checkin_status = checkin_status;
        this.date = date;
        this.time = time;
        this.comments = comments;
        this.prescriptions = prescriptions;
        this.treatments = treatments;
    }

    //without id
    public DetailedAppointment(String patient_name, String staff_name, String allocated_by_staff_name, String status, String checkin_status, String date, String time, String comments, String prescriptions, String treatments) {
        this.patient_name = patient_name;
        this.staff_name = staff_name;
        this.allocated_by_staff_name = allocated_by_staff_name;
        this.status = status;
        this.checkin_status = checkin_status;
        this.date = date;
        this.time = time;
        this.comments = comments;
        this.prescriptions = prescriptions;
        this.treatments = treatments;
    }
    
    //toString
    @Override
    public String toString() {
        return "Detailed Appointment {" + 
                "id=" + id + ", patient_name=" + patient_name + ", staff_name=" + staff_name +
                ", allocated_by_staff_name=" + allocated_by_staff_name + ", status=" + status + 
                ", checkin_status=" + checkin_status + ", date=" + date + ", time=" + time + 
                ", comments=" + comments + ", prescriptions=" + prescriptions + ", treatments=" + treatments + '}';
    }
    
    //Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getStaff_name() {
        return staff_name;
    }

    public void setStaff_name(String staff_name) {
        this.staff_name = staff_name;
    }

    public String getAllocated_by_staff_name() {
        return allocated_by_staff_name;
    }

    public void setAllocated_by_staff_name(String allocated_by_staff_name) {
        this.allocated_by_staff_name = allocated_by_staff_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCheckin_status() {
        return checkin_status;
    }

    public void setCheckin_status(String checkin_status) {
        this.checkin_status = checkin_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(String prescriptions) {
        this.prescriptions = prescriptions;
    }

    public String getTreatments() {
        return treatments;
    }

    public void setTreatments(String treatments) {
        this.treatments = treatments;
    }
    
            
}
