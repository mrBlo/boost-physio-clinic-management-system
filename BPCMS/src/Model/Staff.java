/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ia16abg
 */
public class Staff extends Person{
    private int department_id;
    private String department_name;
    private int staff_id;
    
    //Constructor
     public Staff() {
    }
 public Staff(int department_id, String name, String phone_number) {
        super(name, phone_number);
        this.department_id = department_id;
    }
    public Staff(int department_id, String name, String phone_number, int staff_id) {
        super(name, phone_number);
        this.staff_id = staff_id;
        this.department_id = department_id;
    }
    public Staff(String department_name, String name, String phone_number, int staff_id) {
        super(name, phone_number);
        this.staff_id = staff_id;
        this.department_name = department_name;
    }
    

    @Override
    public String toString() {
        if (staff_id==0 && super.getName().isEmpty() && super.getPhone_number().isEmpty() && department_id==0) {
            return "";
        }
        return "\nSTAFF DETAILS{\nStaff ID : " +staff_id+ "\nDepartment  : " + department_name + 
                "\nName : " +super.getName() +
                 "\nPhone Number : " +super.getPhone_number()
                +"\n}";
    }
    
   //Getters and Setters
    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }
   
    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }
  
    
    
}
