/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author AFRIFA
 */
public class Department {
     private int id;
    private String name;

    //Constructors
    public Department() {
    }

    public Department(int id, String name) {
        this.id = id;
       this.name = name;
    }
    
    //toString
    @Override
    public String toString() {
          if (id==0 && name==null && name==null ) {
         return "";
        }
        return "\nDEPARTMENT DETAILS{\n" + "ID : " + id  + "Department Name : " + name + "\n}";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
