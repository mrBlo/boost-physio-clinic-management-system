/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author ia16abg
 */
public class Appointment {

    private int id, patient_id, allocated_staff_id, allocated_by_staff_id, status, checkin_status;
    private String date, time, comments, prescription_ids, treatment_ids;

    //Constructor
    public Appointment() {
    }
    //Constructor with no ID
    public Appointment(int patient_id, int allocated_staff_id, int allocated_by_staff_id,
            int status, int checkin_status, String date, String time, String comments,
            String prescription_ids, String treatment_ids) {
        this.patient_id = patient_id;
        this.allocated_staff_id = allocated_staff_id;
        this.allocated_by_staff_id = allocated_by_staff_id;
        this.status = status;
        this.checkin_status = checkin_status;
        this.date = date;
        this.time = time;
        this.comments = comments;
        this.prescription_ids = prescription_ids;
        this.treatment_ids = treatment_ids;
    }
        
    //Constructor with ID
    public Appointment(int id, int patient_id, int allocated_staff_id,
            int allocated_by_staff_id, String prescription_ids, String treatment_ids,
            int status, int checkin_status, String date, String time, String comments) {
        this.id = id;
        this.patient_id = patient_id;
        this.allocated_staff_id = allocated_staff_id;
        this.allocated_by_staff_id = allocated_by_staff_id;
        this.prescription_ids = prescription_ids;
        this.treatment_ids = treatment_ids;
        this.status = status;
        this.checkin_status = checkin_status;
        this.date = date;
        this.time = time;
        this.comments = comments;
    }

    
    

    //toString
    @Override
    public String toString() {
           if (id==0 ) {
         return "";
        }
        return "APPOINTMENT DETAILS{\n" + "id=" + id + ", patient_id=" + patient_id
                + ", allocated_staff_id=" + allocated_staff_id + ", allocated_by_staff_id=" + allocated_by_staff_id
                + ", prescription_ids=" + prescription_ids + ", treatment_ids=" + treatment_ids + ", status=" + status
                + ", check in status=" + checkin_status + ", date=" + date + " , time=" + time + " , comments=" + comments + '}';
    }

    //Equals
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Appointment other = (Appointment) obj;
        if (this.patient_id != other.patient_id && this.allocated_staff_id != other.allocated_staff_id && !Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    //Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public int getAllocated_staff_id() {
        return allocated_staff_id;
    }

    public void setAllocated_staff_id(int allocated_staff_id) {
        this.allocated_staff_id = allocated_staff_id;
    }

    public int getAllocated_by_staff_id() {
        return allocated_by_staff_id;
    }

    public void setAllocated_by_staff_id(int allocated_by_staff_id) {
        this.allocated_by_staff_id = allocated_by_staff_id;
    }

    public String getPrescription_ids() {
        return prescription_ids;
    }

    public void setPrescription_ids(String prescription_ids) {
        this.prescription_ids = prescription_ids;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTreatment_ids() {
        return treatment_ids;
    }

    public void setTreatment_ids(String treatment_ids) {
        this.treatment_ids = treatment_ids;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCheckin_status() {
        return checkin_status;
    }

    public void setCheckin_status(int checkin_status) {
        this.checkin_status = checkin_status;
    }

}
