/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;
/**
 *
 * @author ia16abg
 */
public class Person {
private String name,phone_number;
    private int person_id;
    
    //Constructors
    public Person() {
    }
     public Person(String name, String phone_number) {
        this.name = name;
        this.phone_number = phone_number;    
    }
    public Person(String name, String phone_number,  int person_id) {
        this.name = name;
        this.phone_number = phone_number;
        this.person_id = person_id;
    }
     /* public Person(String name, String phone_number, int id,  int booking_id) {
        this.name = name;
        this.phone_number = phone_number;
        this.id = id;
          this.id = booking_id;
    }*/
    
    
    
    //toString
    @Override
    public String toString() {
        if (person_id==0 && name==null && phone_number==null ) {
         return "";
        }
        return "\nPERSON DETAILS{\nPatient ID : " +person_id+ 
                "\nName : " +name+
                "\nPhone Number : " +phone_number 
                +"\n}";
    }
    
    //Equals Method
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.person_id != other.person_id &&  !Objects.equals(this.name, other.name) ) {
            return false;
        }
        
        return true;
    }
    
    //Empty
       //Check if Empty Debtor
    public boolean isEmpty(Person sample) {
        //check null sample
        if (sample==null) {
            return false;
        }
        //Check using  name and Phone
        if (sample.getName().isEmpty() && sample.getPhone_number().isEmpty()) {
            return true;
        }
        return false;
    }
    
    
    
    //Getters and Setters

    public String getName() {
        if (name==null) {
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
           if (phone_number==null) {
            return "";
        }
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

  
   
        
}
