/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author AFRIFA
 */
public class Report {
    private int patientId,no_of_appointments,no_of_attended,no_of_missed,no_of_cancelled,no_of_products_ordered;
    private String patientName;
    
    //Constructor
    public Report() {
    }

    public Report(int patientId, int no_of_appointments, int no_of_attended, int no_of_missed, int no_of_cancelled, int no_of_products_ordered, String patientName) {
        this.patientId = patientId;
        this.no_of_appointments = no_of_appointments;
        this.no_of_attended = no_of_attended;
        this.no_of_missed = no_of_missed;
        this.no_of_cancelled = no_of_cancelled;
        this.no_of_products_ordered = no_of_products_ordered;
        this.patientName = patientName;
    }

  
    
//To String

    @Override
    public String toString() {
            if (patientId==0 && no_of_appointments==0 && no_of_attended==0
                    && no_of_missed==0 && no_of_cancelled==0 
                    && no_of_products_ordered==0) {
         return "";
        }
        return "Report{" + "patientId=" + patientId + ", no_of_appointments=" + no_of_appointments
                + ", no_of_attended=" + no_of_attended + ", no_of_missed=" + no_of_missed + ", no_of_cancelled=" + no_of_cancelled + ", no_of_products_ordered=" + no_of_products_ordered + ", patientName=" + patientName + '}';
    }

    //Getters and Setters
    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getNo_of_appointments() {
        return no_of_appointments;
    }

    public void setNo_of_appointments(int no_of_appointments) {
        this.no_of_appointments = no_of_appointments;
    }

    public int getNo_of_attended() {
        return no_of_attended;
    }

    public void setNo_of_attended(int no_of_attended) {
        this.no_of_attended = no_of_attended;
    }
    

    public int getNo_of_missed() {
        return no_of_missed;
    }

    public void setNo_of_missed(int no_of_missed) {
        this.no_of_missed = no_of_missed;
    }

    public int getNo_of_cancelled() {
        return no_of_cancelled;
    }

    public void setNo_of_cancelled(int no_of_cancelled) {
        this.no_of_cancelled = no_of_cancelled;
    }

    public int getNo_of_products_ordered() {
        return no_of_products_ordered;
    }

    public void setNo_of_products_ordered(int no_of_products_ordered) {
        this.no_of_products_ordered = no_of_products_ordered;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
    
    
    
}
