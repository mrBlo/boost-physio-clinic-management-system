/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;
/**
 *
 * @author ia16abg
 */
public class Appointment {
    private int id,patient_id,allocated_staff_id,allocated_by_staff_id,prescription_id,closed;
    private String date,comments;
    
    //Constructor
    public Appointment() {
    }
    public Appointment(int id, int patient_id, int allocated_staff_id,
            int allocated_by_staff_id, int prescription_id, int closed, String date, String comments) {
        this.id = id;
        this.patient_id = patient_id;
        this.allocated_staff_id = allocated_staff_id;
        this.allocated_by_staff_id = allocated_by_staff_id;
        this.prescription_id = prescription_id;
        this.closed = closed;
        this.date = date;
        this.comments = comments;
    }
   
    //toString
    @Override
    public String toString() {
        return "APPOINTMENT DETAILS{\n" + "id=" + id + ", patient_id=" + patient_id + ", allocated_staff_id=" + allocated_staff_id + ", allocated_by_staff_id=" + allocated_by_staff_id + ", prescription_id=" + prescription_id + ", closed=" + closed + ", date=" + date + ", comments=" + comments + '}';
    }
    
    //Equals
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Appointment other = (Appointment) obj;
        if (this.patient_id != other.patient_id && this.allocated_staff_id != other.allocated_staff_id && !Objects.equals(this.date, other.date)) {
            return false;
        }
           return true;
    }
    
        
    //Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public int getAllocated_staff_id() {
        return allocated_staff_id;
    }

    public void setAllocated_staff_id(int allocated_staff_id) {
        this.allocated_staff_id = allocated_staff_id;
    }

    public int getAllocated_by_staff_id() {
        return allocated_by_staff_id;
    }

    public void setAllocated_by_staff_id(int allocated_by_staff_id) {
        this.allocated_by_staff_id = allocated_by_staff_id;
    }

    public int getPrescription_id() {
        return prescription_id;
    }

    public void setPrescription_id(int prescription_id) {
        this.prescription_id = prescription_id;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    
    
}
