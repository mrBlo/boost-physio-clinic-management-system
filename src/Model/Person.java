/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Objects;

/**
 *
 * @author ia16abg
 */
public class Person {
    private String first_name,last_name,phone_number,address;
    private int id;
    
    //Constructors
    public Person() {
    }

    public Person(String first_name, String last_name, String phone_number, String address, int id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.address = address;
        this.id = id;
    }
    
    //toString
    @Override
    public String toString() {
        return "\nPERSON DETAILS{\nID : " +id+ 
                "\nFirst Name : " +first_name+ "\nLast Name :" + last_name +
                 "\nPhone Number : " +phone_number+ "\nAddress :" + address
                +"\n}";
    }
    
    //Equals Method
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.id != other.id &&!Objects.equals(this.first_name, other.first_name) && !Objects.equals(this.last_name, other.last_name) ) {
            return false;
        }
        
        return true;
    }
    
    
    //Getters and Setters
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    
    
}
