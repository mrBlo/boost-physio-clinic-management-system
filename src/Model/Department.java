/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ia16abg
 */
public class Department {
    private int id,staff_id;
    private String name;

    //Constructors
    public Department() {
    }

    public Department(int id, int staff_id, String name) {
        this.id = id;
        this.staff_id = staff_id;
        this.name = name;
    }
    
    //toString
    @Override
    public String toString() {
        return "\nDEPARTMENT DETAILS{\n" + "ID : " + id + "\nStaff ID : " + staff_id + "Department Name : " + name + "\n}";
    }
    
    
    
    
}
