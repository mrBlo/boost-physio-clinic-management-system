/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ia16abg
 */
public class Staff extends Person{
    private int department_id;
    
    //Constructor
     public Staff() {
    }

    public Staff(int department_id, String first_name, String last_name, String phone_number, String address, int id) {
        super(first_name, last_name, phone_number, address, id);
        this.department_id = department_id;
    }

    @Override
    public String toString() {
        return "\nPHYSIOTHERAPIST DETAILS{\nID : " +super.getId()+ "\nDepartment ID :" + department_id + 
                "\nFirst Name : " +super.getFirst_name()+ "\nLast Name :" + super.getLast_name() +
                 "\nPhone Number : " +super.getPhone_number()+ "\nAddress :" + super.getAddress()
                +"\n}";
    }
    
   //Getters and Setters

    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }
  
    
    
}
