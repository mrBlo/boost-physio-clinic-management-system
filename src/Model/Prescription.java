/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ia16abg
 */
public class Prescription {
    private int id;
    private String name;
    
    
    //Constructor
    public Prescription() {
    }

    public Prescription(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    //toString
    @Override
    public String toString() {
        return "PRESCRIPTION DETAILS {" + "ID : " + id + ", Name : " + name + "\n}";
    }
    
    
    
}
